package com.webcheckers.appl;

import static org.junit.jupiter.api.Assertions.*;

import com.webcheckers.model.Player;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Application-tier")
class PlayerLobbyTest {

  @Test
  void canAddPlayer() {
    String playerString = "player";
    PlayerLobby testLobby = new PlayerLobby();

    assertTrue(testLobby.canAddPlayer(playerString));
  }

  @Test
  void cannotAddPlayerTwice() {
    Player player1 = new Player("player1");

    PlayerLobby testLobby = new PlayerLobby();
    testLobby.signIn(player1);

    assertFalse(testLobby.canAddPlayer(player1.getName()));
  }

  @Test
  void cannotAddInvalidPlayer() {
    PlayerLobby testLobby = new PlayerLobby();

    assertFalse(testLobby.canAddPlayer("@!@#!@#"));
  }

  @Test
  void cannotAddNullPlayer() {
    PlayerLobby testLobby = new PlayerLobby();
    assertFalse(testLobby.canAddPlayer(null));
  }

  @Test
  void signIn() {
    Player player = new Player("player");
    PlayerLobby testLobby = new PlayerLobby();
    testLobby.signIn(player);

    assertTrue(testLobby.getPlayers().contains(player));
  }

  @Test
  void addMultiplePeople() {
    Player firstPlayer = new Player("player 1");
    Player secondPlayer = new Player("player 2");

    PlayerLobby testLobby = new PlayerLobby();
    testLobby.signIn(firstPlayer);
    testLobby.signIn(secondPlayer);

    assertEquals(2, testLobby.getPlayers().size());
  }

  @Test
  void getPlayerByName() {
    Player player = new Player("player");

    PlayerLobby testLobby = new PlayerLobby();
    testLobby.signIn(player);
    assertEquals(testLobby.getPlayer("player"), player);
  }

  @Test
  void getPlayerNull() {
    Player player = new Player("player");
    PlayerLobby testLobby = new PlayerLobby();

    testLobby.signIn(player);
    assertNull(testLobby.getPlayer("player5"));
  }

  @Test
  void sameName() {
    Player firstPlayer = new Player("player1");
    Player secondPlayer = new Player("player1");
    PlayerLobby testLobby = new PlayerLobby();

    testLobby.signIn(firstPlayer);

    assertThrows(IllegalArgumentException.class, () -> testLobby.signIn(secondPlayer));
  }

  @Test
  void signOut() {
    Player player = new Player("player");
    PlayerLobby testLobby = new PlayerLobby();

    testLobby.signIn(player);
    testLobby.signOut(player);
    assertNull(testLobby.getPlayer("player"));
  }
}
