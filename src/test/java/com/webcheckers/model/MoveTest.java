package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.webcheckers.model.view.Piece.Color;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class MoveTest {

  @Test
  void flipMove() {
    Position start = new Position(2, 2);
    Position end = new Position(3, 3);
    Move move = new Move(start, end);

    Move flippedMove = move.fromPlayerPerspective(Color.WHITE);
    Position flippedStart = flippedMove.getStart();
    Position flippedEnd = flippedMove.getEnd();

    assertEquals(5, flippedStart.getRow());
    assertEquals(5, flippedStart.getCell());

    assertEquals(4, flippedEnd.getRow());
    assertEquals(4, flippedEnd.getCell());
  }

  @Test
  void moveTypeSimple() {
    Position start = new Position(3, 3);
    Position end = new Position(4, 4);

    Move move = new Move(start, end);
    assertEquals(MoveType.SIMPLE, move.getMoveType());
  }

  @Test
  void moveTypeJump() {
    Position start = new Position(3, 3);
    Position end = new Position(5, 5);

    Move move = new Move(start, end);
    assertEquals(MoveType.JUMP, move.getMoveType());
  }

  @Test
  void getJumpedPosition() {
    Position start = new Position(3, 3);
    Position end = new Position(5, 5);

    Move move = new Move(start, end);
    Position jumped = move.getJumpedPosition();
    assertEquals(4, jumped.getRow());
    assertEquals(4, jumped.getCell());
  }

  @Test
  void getSimpleJumpedPosition() {
    Position start = new Position(4, 4);
    Position end = new Position(5, 5);

    Move move = new Move(start, end);
    Position jumped = move.getJumpedPosition();
    assertNull(jumped);
  }
}
