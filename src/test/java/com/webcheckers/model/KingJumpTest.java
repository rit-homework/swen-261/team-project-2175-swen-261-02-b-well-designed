package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertNull;

import com.webcheckers.model.view.Piece.Color;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class KingJumpTest {

  private String JUMP_OTHER_PLAYER =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            " r      ",
            "w       "
          });

  @Test
  void jumpByColor() {
    Board board = Board.fromString(JUMP_OTHER_PLAYER);

    Position start = new Position(7, 0);
    board.getPiece(start).makeKing();

    Move move = new Move(start, new Position(5, 2));
    assertNull(MoveValidator.validateMove(move, board, Color.WHITE));
  }
}
