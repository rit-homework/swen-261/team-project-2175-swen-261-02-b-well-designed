package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.webcheckers.model.view.Piece.Color;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class IsJumpPossibleTest {

  private String CHECK_JUMP_RED_RIGHT =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            " w      ",
            "r       "
          });

  private String CHECK_JUMP_RED_LEFT =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "      w ",
            "       r",
            "        "
          });

  private String CHECK_JUMP_WHITE_RIGHT =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "  w     ",
            " r      ",
            "        "
          });

  private String CHECK_JUMP_WHITE_LEFT =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "  w     ",
            " r      ",
            "        "
          });

  private String CHECK_JUMP_NOT_AVAILABLE_CLOSE =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "  r     ",
            " w      ",
            "        "
          });

  private String CHECK_JUMP_NOT_AVAILABLE_FAR =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            " w      ",
            "        ",
            "        ",
            "        ",
            " r      ",
            "        "
          });

  /** Makes sure that a red piece's right jump can be detected */
  @Test
  void jumpRedRight() {
    Board board = Board.fromString(CHECK_JUMP_RED_RIGHT);
    assertTrue(MoveValidator.isJumpPossible(Color.RED, board));
  }

  /** Makes sure a red piece's left jump can be detected */
  @Test
  void jumpRedLeft() {
    Board board = Board.fromString(CHECK_JUMP_RED_LEFT);
    assertTrue(MoveValidator.isJumpPossible(Color.RED, board));
  }

  /** Makes sure a white piece's right jump can be detected */
  @Test
  void jumpWhiteRight() {
    Board board = Board.fromString(CHECK_JUMP_WHITE_RIGHT);
    assertTrue(MoveValidator.isJumpPossible(Color.WHITE, board));
  }

  /** Makes sure a white piece's left jump can be detected */
  @Test
  void jumpWhiteLeft() {
    Board board = Board.fromString(CHECK_JUMP_WHITE_LEFT);
    assertTrue(MoveValidator.isJumpPossible(Color.WHITE, board));
  }

  /** Checks to see that a jump will not be detected if it requires a piece to move backwards */
  @Test
  void jumpNotAvailableClose() {
    Board board = Board.fromString(CHECK_JUMP_NOT_AVAILABLE_CLOSE);
    assertFalse(MoveValidator.isJumpPossible(Color.WHITE, board));
  }

  /** Checks to make sure a jump is detected when there is none */
  @Test
  void jumpNotAvailableFar() {
    Board board = Board.fromString(CHECK_JUMP_NOT_AVAILABLE_FAR);
    assertFalse(MoveValidator.isJumpPossible(Color.RED, board));
  }
}
