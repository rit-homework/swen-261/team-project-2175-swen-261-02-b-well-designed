package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.webcheckers.model.view.Piece;
import com.webcheckers.model.view.Piece.Color;
import com.webcheckers.model.view.Piece.Type;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class BoardTest {

  private String INITIAL_BOARD =
      String.join(
          "\n",
          " w w w w",
          "w w w w ",
          " w w w w",
          "        ",
          "        ",
          "r r r r ",
          " r r r r",
          "r r r r ");

  private String TOO_MANY_LINES =
      String.join(
          "\n",
          " w w w w",
          "w w w w ",
          " w w w w",
          " w w w w",
          " w w w w",
          "        ",
          "        ",
          "r r r r ",
          " r r r r",
          "r r r r ");

  private String TOO_MANY_COLS =
      String.join(
          "\n",
          " w w w w",
          "w w w w ",
          " w w w w r w r w",
          "        ",
          "        ",
          "r r r r ",
          " r r r r",
          "r r r r ");

  @Test
  void testInitialState() {
    Board board = Board.createBoard();

    assertEquals(INITIAL_BOARD, board.toString());
  }

  @Test
  void testFromString() {
    Board board = Board.fromString(INITIAL_BOARD);

    assertEquals(INITIAL_BOARD, board.toString());
  }

  @Test
  void fromStringTooManyLines() {
    assertThrows(IllegalArgumentException.class, () -> Board.fromString(TOO_MANY_LINES));
  }

  @Test
  void fromStringTooManyCols() {
    assertThrows(IllegalArgumentException.class, () -> Board.fromString(TOO_MANY_COLS));
  }

  @Test
  void getOutOfBoundPiece() {
    Position position = new Position(100, 100);

    Board board = Board.fromString(INITIAL_BOARD);
    Piece piece = board.getPiece(position);

    assertNull(piece);
  }

  @Test
  void setOutOfBoundPiece() {
    Position position = new Position(100, 100);

    Board board = Board.fromString(INITIAL_BOARD);

    assertThrows(IllegalArgumentException.class, () -> board.setPiece(position, null));
  }

  @Test
  void applySimpleMove() {
    Board board = Board.fromString(INITIAL_BOARD);

    Position start = new Position(5, 0);
    Piece startingPiece = board.getPiece(start);

    Position end = new Position(4, 1);
    Move move = new Move(start, end);

    board.applyMove(move);

    assertEquals(startingPiece, board.getPiece(end));
    assertNull(board.getPiece(start));
  }

  @Test
  void applyJumpMove() {
    String JUMP_BOARD =
        String.join(
            "\n",
            " w w w w",
            "w w w w ",
            " w   w w",
            "        ",
            " w      ",
            "r r r r ",
            " r r r r",
            "r r r r ");

    Board board = Board.fromString(JUMP_BOARD);

    Position start = new Position(5, 0);
    Piece startingPiece = board.getPiece(start);

    Position end = new Position(3, 2);
    Move move = new Move(start, end);

    board.applyMove(move);

    assertEquals(startingPiece, board.getPiece(end));
    assertNull(board.getPiece(start));
    assertNull(board.getPiece(move.getJumpedPosition()));
  }

  @Test
  void applyKingedMove() {
    String JUMP_BOARD =
        String.join(
            "\n",
            " w w w w",
            "w w w w ",
            "     w w",
            "        ",
            "        ",
            "        ",
            " w      ",
            "        ");

    Board board = Board.fromString(JUMP_BOARD);

    Position start = new Position(6, 1);
    Piece startingPiece = board.getPiece(start);

    Position end = new Position(7, 0);
    Move move = new Move(start, end);

    board.applyMove(move);
    board.kingPieces(Color.WHITE);

    assertEquals(startingPiece, board.getPiece(end));
    assertNull(board.getPiece(start));
    assertEquals(Piece.Type.KING, startingPiece.getType());
  }

  @Test
  void undoJumpMove() {
    String boardString =
        String.join(
            "\n",
            " w w w w",
            "w w w w ",
            " w   w w",
            "    r   ",
            "        ",
            "r r r   ",
            " r r r r",
            "r r r r ");

    Board board = Board.fromString(boardString);

    Position start = new Position(5, 6);
    Position end = new Position(3, 4);

    Move move = new Move(start, end);

    board.undoMove(move);

    String finalBoard =
        String.join(
            "\n",
            " w w w w",
            "w w w w ",
            " w   w w",
            "        ",
            "     w  ",
            "r r r r ",
            " r r r r",
            "r r r r ");

    assertEquals(finalBoard, board.toString());
  }

  @Test
  void undoKingMove() {
    String boardString =
        String.join(
            "\n",
            "        ",
            "r       ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ");

    Board board = Board.fromString(boardString);

    Position start = new Position(1, 0);
    Position end = new Position(0, 1);
    Move move = new Move(start, end);

    board.applyMove(move);
    board.undoMove(move);

    assertEquals(boardString, board.toString());
    assertEquals(Type.SINGLE, board.getPiece(start).getType());
  }

  @Test
  void ensurePieceAtPosition() {
    Board board = Board.fromString(INITIAL_BOARD);

    board.ensurePieceAtPosition(new Position(0, 1));
  }

  @Test
  void ensurePieceNotAtPosition() {
    Board board = Board.fromString(INITIAL_BOARD);

    assertThrows(
        IllegalArgumentException.class, () -> board.ensurePieceAtPosition(new Position(0, 0)));
  }
}
