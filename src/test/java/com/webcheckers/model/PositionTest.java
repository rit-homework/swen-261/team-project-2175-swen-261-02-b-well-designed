package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class PositionTest {

  @Test
  void flipPosition() {
    Position unFlipped = new Position(7, 7);
    Position flipped = unFlipped.flip();

    assertEquals(0, flipped.getRow());
    assertEquals(0, flipped.getCell());
  }

  @Test
  void testEquals() {
    Position position = new Position(0, 1);
    Position position2 = new Position(0, 1);

    Move move = new Move(position, position);
    assertEquals(position, position);
    assertEquals(position, position2);

    assertNotEquals(position, move);
  }

  @Test
  void testHashCode() {
    Position position = new Position(0, 1);
    Position position2 = new Position(0, 1);
    assertEquals(position.hashCode(), position2.hashCode());
  }
}
