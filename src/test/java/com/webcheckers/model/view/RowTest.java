package com.webcheckers.model.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.webcheckers.model.Board;
import com.webcheckers.model.view.Piece.Color;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class RowTest {

  @Test
  void invalidRowSize() {
    Piece[] rawRow = new Piece[] {new Piece(Color.WHITE), null, new Piece(Color.RED)};

    assertThrows(IllegalArgumentException.class, () -> new Row(rawRow, 0));
  }

  @Test
  void invalidRowIdx() {
    Piece[] rawRow = new Piece[Board.BOARD_COLS];

    assertThrows(IllegalArgumentException.class, () -> new Row(rawRow, Board.BOARD_ROWS));
  }

  @Test
  void validRow() {
    Piece[] rawRow =
        new Piece[] {
          new Piece(Color.RED), null, null, null, null, null, null, new Piece(Color.RED)
        };

    Row row = new Row(rawRow, 0);

    List<Piece> actualPieces =
        StreamSupport.stream(row.spliterator(), false)
            .map(Space::getPiece)
            .collect(Collectors.toList());

    assertEquals(Arrays.asList(rawRow), actualPieces);
  }

  @Test
  void getIndexGetter() {
    int rowIdx = 0;
    Piece[] rawRow =
        new Piece[] {
          new Piece(Color.RED), null, null, null, null, null, null, new Piece(Color.RED)
        };
    Row row = new Row(rawRow, rowIdx);

    assertEquals(rowIdx, row.getIndex());
  }
}
