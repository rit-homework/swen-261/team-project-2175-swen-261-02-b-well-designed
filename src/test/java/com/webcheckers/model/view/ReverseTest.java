package com.webcheckers.model.view;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.Arrays;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class ReverseTest {

  @Test
  void testReverseEven() {
    Integer actual[] = {1, 3, 7, 9, 11, 28};
    BoardView.reverse(actual);

    int expected[] = {28, 11, 9, 7, 3, 1};

    assertArrayEquals(expected, unboxArray(actual));
  }

  @Test
  void testReverseOdd() {
    Integer actual[] = {1, 3, 7, 9, 11, 28, 31};
    BoardView.reverse(actual);

    int expected[] = {31, 28, 11, 9, 7, 3, 1};

    assertArrayEquals(expected, unboxArray(actual));
  }

  private int[] unboxArray(Integer[] elems) {
    return Arrays.stream(elems).mapToInt(elem -> elem).toArray();
  }
}
