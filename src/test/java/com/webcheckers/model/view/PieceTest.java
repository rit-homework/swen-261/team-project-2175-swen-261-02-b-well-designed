package com.webcheckers.model.view;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.webcheckers.model.view.Piece.Color;
import com.webcheckers.model.view.Piece.Type;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class PieceTest {

  @Test
  void kingPawn() {
    Piece piece = new Piece(Color.RED);

    piece.makeKing();
    assertEquals(Type.KING, piece.getType());
  }

  @Test
  void kingKing() {
    Piece piece = new Piece(Color.RED);

    piece.makeKing();
    piece.makeKing();
    assertEquals(Type.KING, piece.getType());
  }

  @Test
  void asStringRed() {
    Piece piece = new Piece(Color.RED);

    assertEquals("r", Piece.asString(piece));
  }

  @Test
  void asStringWhite() {
    Piece piece = new Piece(Color.WHITE);

    assertEquals("w", Piece.asString(piece));
  }

  @Test
  void asStringNullType() {
    Piece piece = new Piece(null);

    assertEquals("!", Piece.asString(piece));
  }

  @Test
  void asStringNull() {
    assertEquals(" ", Piece.asString(null));
  }
}
