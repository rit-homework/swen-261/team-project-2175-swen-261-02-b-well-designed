package com.webcheckers.model.view;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.webcheckers.model.Board;
import com.webcheckers.model.view.Piece.Color;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class SpaceTest {

  @Test
  void checkerBoard() {
    String actualBoard =
        IntStream.range(0, Board.BOARD_ROWS)
            .mapToObj(
                rowIdx ->
                    IntStream.range(0, Board.BOARD_COLS)
                        .mapToObj(colIdx -> Space.isBlackSquare(rowIdx, colIdx) ? "B" : " ")
                        .collect(Collectors.joining()))
            .collect(Collectors.joining("\n"));

    String expected =
        String.join(
            "\n",
            " B B B B",
            "B B B B ",
            " B B B B",
            "B B B B ",
            " B B B B",
            "B B B B ",
            " B B B B",
            "B B B B ");

    assertEquals(expected, actualBoard);
  }

  @Test
  void testGetters() {
    Piece piece = new Piece(Color.WHITE);
    int cellIdx = 3;

    Space space = new Space(3, piece, true);

    assertEquals(cellIdx, space.getCellIdx());
    assertEquals(piece, space.getPiece());
  }

  @Test
  void testIsValid() {
    Space space = new Space(3, null, true);
    assertTrue(space.isValid());
  }

  @Test
  void testInvalidBecauseWhiteSquare() {
    Space space = new Space(3, null, false);
    assertFalse(space.isValid());
  }

  @Test
  void testInvalidBecauseOccupied() {
    Piece piece = new Piece(Color.WHITE);

    Space space = new Space(3, piece, false);
    assertFalse(space.isValid());
  }
}
