package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class PlayerGameStateTest {
  private final Player player = new Player("test");
  private final PlayerGameState state = new PlayerGameState(player);

  @Test
  void testStoreMove() {
    Move move = new Move(null, null);
    state.pushLastValidatedMove(move);
    assertEquals(move, state.popLastValidatedMove());
  }

  @Test
  void testEmptyMoveStack() {
    Move move = new Move(null, null);

    state.pushLastValidatedMove(move);
    state.emptyMoveStack();
    assertTrue(state.isLastValidatedMoveStackEmpty());
  }
}
