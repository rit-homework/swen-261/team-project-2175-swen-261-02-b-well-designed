package com.webcheckers.model.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.MoveType;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Piece.Color;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class JumpRulesTest {

  private String CHECK_NO_PIECE_TO_JUMP =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "r       "
          });

  private String CHECK_JUMP_DISTANCE =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            " w      ",
            "r       "
          });

  private String CHECK_CANT_JUMP_SAME_COLOR_PIECE =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            " r      ",
            "r       "
          });

  /** Checks what should be a valid jump through jump ruleset */
  @Test
  void validJump() {
    Board board = Board.fromString(CHECK_JUMP_DISTANCE);
    Move move = new Move(new Position(7, 0), new Position(5, 2));

    assertNull(Rule.violatesOneOf(move, board, Color.RED, MoveType.JUMP.getRules()));
  }

  /** Specifically tests when there is no piece to jump, testing the JumpPieceRule class */
  @Test
  void noPieceToJump() {
    Board board = Board.fromString(CHECK_NO_PIECE_TO_JUMP);
    Move move = new Move(new Position(7, 0), new Position(5, 2));

    assertEquals(
        MoveInvalidReason.NO_PIECE_TO_JUMP,
        Rule.violatesOneOf(move, board, Color.RED, MoveType.JUMP.getRules()));
  }

  /**
   * Specifically tests a piece's jump distance is valid ( = 2 spaces each direction), tests the
   * JumpOneSpaceRule class
   */
  @Test
  void jumpDistance() {
    Board board = Board.fromString(CHECK_JUMP_DISTANCE);
    Move move = new Move(new Position(7, 0), new Position(3, 2));

    assertEquals(
        MoveInvalidReason.JUMP_NOT_TWO_SPACES,
        Rule.violatesOneOf(move, board, Color.RED, MoveType.JUMP.getRules()));
  }

  /** Makes sure a piece cannot jump one of it's own color, further tests JumpPieceRule class */
  @Test
  void jumpByColor() {
    Board board = Board.fromString(CHECK_CANT_JUMP_SAME_COLOR_PIECE);

    Move move = new Move(new Position(7, 0), new Position(5, 2));

    assertEquals(
        MoveInvalidReason.JUMP_PIECE_SAME_COLOR,
        Rule.violatesOneOf(move, board, Color.RED, MoveType.JUMP.getRules()));
  }
}
