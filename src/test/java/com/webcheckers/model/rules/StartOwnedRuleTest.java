package com.webcheckers.model.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Piece.Color;
import org.junit.jupiter.api.Test;

class StartOwnedRuleTest {
  @Test
  void testEmpty() {
    Board board =
        Board.fromString(
            String.join(
                "\n",
                " w w w w",
                "w w w w ",
                " w w w w",
                "        ",
                "        ",
                "r r r r ",
                " r r r r",
                "r r r r "));

    Position start = new Position(3, 0);
    Position end = new Position(4, 1);
    Move move = new Move(start, end);

    StartOwnedRule rule = new StartOwnedRule();
    assertEquals(MoveInvalidReason.NO_START_PIECE, rule.checkMove(move, Color.RED, board));
  }
}
