package com.webcheckers.model.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.Position;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class StartBoundsRuleTest {
  private static final StartBoundsRule ruleCheck = new StartBoundsRule();

  @Test
  void testGoodStart() {
    Move goodStart = new Move(new Position(0, 0), new Position(1, 1));
    assertNull(ruleCheck.checkMove(goodStart, null, null));
  }

  @Test
  void testBadStart() {
    Move badStart = new Move(new Position(-1, 0), new Position(0, 1));
    assertEquals(ruleCheck.checkMove(badStart, null, null), MoveInvalidReason.START_BOUNDS);
  }
}
