package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class PlayerTest {

  private static final Player player1 = new Player("Rick");
  private static final Player player2 = new Player("Bob");
  private static final Game game = Game.createGame(player1, player2);

  @Test
  void testPlayerNameEquals() {
    Player player = new Player("Rick");
    assertEquals(player, player1);
  }

  @Test
  void testPlayerNameNotEquals() {
    Player player = new Player("Nope");
    assertNotEquals(player, player1);
  }

  @Test
  void invalidName() {
    assertFalse(Player.isNameValid("#!@#!@"));
  }

  @Test
  void validName() {
    assertTrue(Player.isNameValid("player 1"));
  }

  @Test
  void invalidConstruction() {
    assertThrows(IllegalArgumentException.class, () -> new Player("!#!@#"));
  }

  @Test
  void checkHashCode() {
    Player one = new Player("first");
    Player two = new Player("first");

    assertEquals(one.hashCode(), two.hashCode());
  }

  @Test
  void differentClassesNotEqual() {
    Player lhs = new Player("left hand side");
    Move move = new Move(null, null);

    assertNotEquals(lhs, move);
  }
}
