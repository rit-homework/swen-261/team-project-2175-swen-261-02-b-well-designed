package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertThrows;

import com.webcheckers.model.view.Piece.Color;
import org.junit.jupiter.api.Test;

class MoveValidatorTest {
  @Test
  void missingRequiredFields() {
    Move move = new Move(null, null);
    Board board = new Board();

    assertThrows(
        IllegalArgumentException.class, () -> MoveValidator.validateMove(move, board, Color.RED));
  }
}
