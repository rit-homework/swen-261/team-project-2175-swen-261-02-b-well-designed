package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.webcheckers.model.view.Piece.Color;
import java.util.stream.Stream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class SimpleMoveTest {

  private String END_OF_BOARD_BOARD =
      String.join(
          "\n",
          new String[] {
            "   r    ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        "
          });

  private String NEAR_OF_BOARD_BOARD =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "    w   "
          });

  /**
   * Runs a test described by a move string.
   *
   * @param state The move string to validate.
   */
  private void testFromMoveString(String state) {
    // Create Board
    Board board = Board.fromString(state);

    // Find Starting Position
    String[] lines = state.split("\n");

    Stream<DoubleIndexedContainer<Character>> chars =
        DoubleIndexedContainer.streamFromStringArray(lines);

    DoubleIndexedContainer<Character> startingPositionContainer =
        chars
            .filter(container -> container.value == 'r' || container.value == 'w')
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("No initial position was found."));

    Position starting = new Position(startingPositionContainer.row, startingPositionContainer.col);

    Color color = startingPositionContainer.value == 'r' ? Color.RED : Color.WHITE;

    StringBuilder builder = new StringBuilder(Board.BOARD_ROWS * (Board.BOARD_COLS + 1));
    for (int rowIdx = 0; rowIdx < Board.BOARD_ROWS; rowIdx++) {
      for (int colIdx = 0; colIdx < Board.BOARD_COLS; colIdx++) {
        if (rowIdx == startingPositionContainer.row && colIdx == startingPositionContainer.col) {
          builder.append(startingPositionContainer.value);
          continue;
        }

        Position to = new Position(rowIdx, colIdx);
        Move move = new Move(starting, to);
        boolean isValid = MoveValidator.validateMove(move, board, color) == null;
        if (isValid) {
          builder.append('v');
        } else {
          builder.append(' ');
        }
      }

      if (rowIdx != Board.BOARD_ROWS - 1) {
        builder.append('\n');
      }
    }

    assertEquals(state, builder.toString());
  }

  @Test
  void testDiagonal() {
    String s =
        String.join(
            "\n",
            "        ",
            "        ",
            "        ",
            "  v v   ",
            "   r    ",
            "        ",
            "        ",
            "        ");

    testFromMoveString(s);
  }

  @Test
  void nopMoveNotAllowed() {
    Board board = Board.createBoard();
    Position start = new Position(5, 0);
    Position end = new Position(5, 0);

    Move move = new Move(start, end);

    boolean isValid = MoveValidator.validateMove(move, board, Color.RED) == null;
    assertFalse(isValid);
  }

  @Test
  void doubleMoveNotAllowed() {
    Board board = Board.createBoard();
    Position start = new Position(5, 4);
    Position end = new Position(3, 2);

    Move move = new Move(start, end);

    boolean isValid = MoveValidator.validateMove(move, board, Color.RED) == null;
    assertFalse(isValid);
  }

  @Test
  void moveOtherColorPiece() {
    Board board = Board.createBoard();
    Position start = new Position(5, 0);
    Position end = new Position(4, 1);

    Move move = new Move(start, end);

    boolean isValid = MoveValidator.validateMove(move, board, Color.WHITE) == null;
    assertFalse(isValid);
  }

  @Test
  void moveOffLeftSideOfBoardNotAllowed() {
    Board board = Board.createBoard();
    Position start = new Position(5, 0);
    Position end = new Position(4, -1);

    Move move = new Move(start, end);

    boolean isValid = MoveValidator.validateMove(move, board, Color.RED) == null;
    assertFalse(isValid);
  }

  @Test
  void moveOffRightSideOfBoardNotAllowed() {
    Board board = Board.createBoard();
    Position start = new Position(6, 7);
    Position end = new Position(5, 8);

    Move move = new Move(start, end);

    boolean isValid = MoveValidator.validateMove(move, board, Color.RED) == null;
    assertFalse(isValid);
  }

  @Test
  void moveRightOffFarEndOfBoardNotAllowed() {
    Board board = Board.fromString(END_OF_BOARD_BOARD);

    Position start = new Position(0, 3);
    Position end = new Position(-1, 4);

    Move move = new Move(start, end);

    boolean isValid = MoveValidator.validateMove(move, board, Color.RED) == null;
    assertFalse(isValid);
  }

  @Test
  void moveLeftOffFarEndOfBoardNotAllowed() {
    Board board = Board.fromString(END_OF_BOARD_BOARD);

    Position start = new Position(0, 3);
    Position end = new Position(-1, 2);

    Move move = new Move(start, end);

    boolean isValid = MoveValidator.validateMove(move, board, Color.RED) == null;
    assertFalse(isValid);
  }

  @Test
  void moveRightOffNearEndOfBoardNotAllowed() {
    Board board = Board.fromString(NEAR_OF_BOARD_BOARD);

    Position start = new Position(7, 4);
    Position end = new Position(8, 5);

    Move move = new Move(start, end);
    boolean isValid = MoveValidator.validateMove(move, board, Color.WHITE) == null;
    assertFalse(isValid);
  }

  @Test
  void moveLeftOffNearEndOfBoardNotAllowed() {
    Board board = Board.fromString(NEAR_OF_BOARD_BOARD);

    Position start = new Position(7, 4);
    Position end = new Position(8, 3);

    Move move = new Move(start, end);
    boolean isValid = MoveValidator.validateMove(move, board, Color.WHITE) == null;
    assertFalse(isValid);
  }

  @Test
  void moveOnOtherPieceNotAllowed() {
    String boardString =
        String.join(
            "\n",
            new String[] {
              "        ",
              "        ",
              "        ",
              "        ",
              "   w    ",
              "    r   ",
              "        ",
              "        "
            });

    Board board = Board.fromString(boardString);

    Position start = new Position(4, 3);
    Position end = new Position(5, 4);

    Move move = new Move(start, end);
    boolean isValid = MoveValidator.validateMove(move, board, Color.WHITE) == null;
    assertFalse(isValid);
  }
}
