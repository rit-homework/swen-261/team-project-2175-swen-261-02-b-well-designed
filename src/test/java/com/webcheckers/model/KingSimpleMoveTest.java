package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertNull;

import com.webcheckers.model.view.Piece.Color;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class KingSimpleMoveTest {
  private String NEAR_OF_BOARD_BOARD =
      String.join(
          "\n",
          new String[] {
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "        ",
            "    r   "
          });

  @Test
  void moveBackwards() {
    Board board = Board.fromString(NEAR_OF_BOARD_BOARD);

    Position start = new Position(7, 4);
    Position end = new Position(6, 3);

    Move move = new Move(start, end);
    assertNull(MoveValidator.validateMove(move, board, Color.RED));
  }
}
