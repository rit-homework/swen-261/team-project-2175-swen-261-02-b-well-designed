package com.webcheckers.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.webcheckers.model.view.Piece.Color;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("Model-tier")
class GameTest {

  private String STARTING_BOARD =
      String.join(
          "\n",
          new String[] {
            " w w w w",
            "w w w w ",
            " w w w w",
            "        ",
            "        ",
            "r r r r ",
            " r r r r",
            "r r r r "
          });

  @Test
  void playerInitialization() {
    Player player1 = new Player("test1");
    Player player2 = new Player("test2");
    Game game = Game.createGame(player1, player2);

    assertEquals(player1, game.getFirstPlayer());
    assertEquals(player2, game.getSecondPlayer());
    assertEquals(player1, game.getActivePlayer());
  }

  @Test
  void playerColor() {
    Player player1 = new Player("test1");
    Player player2 = new Player("test2");
    Game game = Game.createGame(player1, player2);

    assertEquals(Color.RED, game.getColor(player1));
    assertEquals(Color.WHITE, game.getColor(player2));
  }

  @Test
  void playerNextTurn() {
    Player player1 = new Player("test1");
    Player player2 = new Player("test2");
    Game game = Game.createGame(player1, player2);

    assertEquals(player1, game.getActivePlayer());
    game.nextTurn();

    assertEquals(player2, game.getActivePlayer());
    game.nextTurn();

    assertEquals(player1, game.getActivePlayer());
  }

  @Test
  void boardInitialization() {
    Player player1 = new Player("test1");
    Player player2 = new Player("test2");
    Game game = Game.createGame(player1, player2);

    assertEquals(STARTING_BOARD, game.getBoard().toString());
  }

  @Test
  void ensurePlayerInGameFail() {
    Player player1 = new Player("test1");
    Player player2 = new Player("test2");
    Game game = Game.createGame(player1, player2);

    Player other = new Player("other");

    assertThrows(IllegalArgumentException.class, () -> game.ensurePlayerInGame(other));
  }

  @Test
  void endGameWithLooser() {
    Player player1 = new Player("test1");
    Player player2 = new Player("test2");
    Game game = Game.createGame(player1, player2);

    game.endGameWithLoser(player1);

    assertEquals(player1, game.getLoser());
    assertEquals(player2, game.getWinner());
    assertFalse(game.isActive());
  }

  @Test
  void endGameWithWinner() {
    Player player1 = new Player("test1");
    Player player2 = new Player("test2");
    Game game = Game.createGame(player1, player2);

    game.endGameWithWinner(player1);

    assertEquals(player1, game.getWinner());
    assertEquals(player2, game.getLoser());
    assertFalse(game.isActive());
  }
}
