package com.webcheckers.ui;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import com.webcheckers.model.view.Message;
import com.webcheckers.model.view.Message.Type;
import com.webcheckers.ui.session.MockSessionWrapper;
import com.webcheckers.ui.session.SessionWrapper;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("UI-tier")
class CheckTurnRouteTest {

  private final GameCenter gameCenter = new GameCenter();
  private final CheckTurnRoute route = new CheckTurnRoute(gameCenter);

  @Test
  void turnCheck() {
    Player player1 = new Player("player1");
    Player player2 = new Player("player2");
    Game game = Game.createGame(player1, player2);

    SessionWrapper session = new MockSessionWrapper(player2);

    Message message = route.handle(session, game);

    assertAll(
        () -> assertEquals("false", message.getText()),
        () -> assertEquals(Type.info, message.getType()));
  }
}
