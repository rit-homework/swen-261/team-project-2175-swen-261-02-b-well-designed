package com.webcheckers.ui;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.gson.Gson;
import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Board;
import com.webcheckers.model.Game;
import com.webcheckers.model.Move;
import com.webcheckers.model.Player;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Message;
import com.webcheckers.model.view.Message.Type;
import com.webcheckers.ui.session.MockSessionWrapper;
import com.webcheckers.ui.session.SessionWrapper;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import spark.RequestResponseFactory;
import spark.Response;

@Tag("UI-tier")
class ValidateMoveRouteTest {
  private final GameCenter gameCenter = new GameCenter();
  private final Gson gson = new Gson();
  private final ValidateMoveRoute route = new ValidateMoveRoute(gson, gameCenter);
  private final MockHttpServletResponse response = new MockHttpServletResponse();
  private final Response sparkResponse = RequestResponseFactory.create(response);

  @Test
  void notTurn() {
    Player firstPlayer = new Player("first");
    Player secondPlayer = new Player("second");
    Game game = Game.createGame(firstPlayer, secondPlayer);

    Position startPosition = new Position(5, 2);
    Position endPosition = new Position(4, 3);
    Move move = new Move(startPosition, endPosition);

    SessionWrapper session = new MockSessionWrapper(secondPlayer);

    Message message = route.handleJson(session, game, move, sparkResponse);
    assertAll(
        () -> assertEquals("It is not your turn yet.", message.getText()),
        () -> assertEquals(Type.error, message.getType()));
  }

  @Test
  void invalidMoveFormat() {
    Player firstPlayer = new Player("first");
    Player secondPlayer = new Player("second");
    Game game = Game.createGame(firstPlayer, secondPlayer);

    Move move = new Move(null, null);

    SessionWrapper session = new MockSessionWrapper(firstPlayer);

    Message message = route.handleJson(session, game, move, sparkResponse);

    assertAll(
        () ->
            assertEquals(
                "The input sent to the server is invalid. A required field is null.",
                message.getText()),
        () -> assertEquals(Type.error, message.getType()));

    assertEquals(400, response.getStatus());
  }

  @Test
  void wrongMove() {
    Player firstPlayer = new Player("first");
    Player secondPlayer = new Player("second");
    Game game = Game.createGame(firstPlayer, secondPlayer);

    SessionWrapper session = new MockSessionWrapper(firstPlayer);

    Position startPosition = new Position(5, 2);
    Position endPosition = new Position(4, 2);
    Move move = new Move(startPosition, endPosition);

    Message message = route.handleJson(session, game, move, sparkResponse);
    assertAll(
        () -> assertNotNull(message.getText()), () -> assertEquals(Type.error, message.getType()));
  }

  @Test
  void okMove() {
    Player firstPlayer = new Player("first");
    Player secondPlayer = new Player("second");
    Game game = Game.createGame(firstPlayer, secondPlayer);

    SessionWrapper session = new MockSessionWrapper(firstPlayer);

    Position startPosition = new Position(5, 2);
    Position endPosition = new Position(4, 3);
    Move move = new Move(startPosition, endPosition);

    Message message = route.handleJson(session, game, move, sparkResponse);
    assertAll(
        () -> assertEquals("That move is OK!", message.getText()),
        () -> assertEquals(Type.info, message.getType()));
  }

  @Test
  void parseEmptyMove() throws Exception {
    Player firstPlayer = new Player("first");
    Player secondPlayer = new Player("second");
    Game.createGame(firstPlayer, secondPlayer);

    MockHttpServletRequest request = new MockHttpServletRequest();
    route.handle(RequestResponseFactory.create(request), sparkResponse);
  }

  @Test
  void multipleJumpMoves() {
    Board board =
        Board.fromString(
            String.join(
                "\n",
                " w w w w",
                "w   w w ",
                " w w w w",
                "        ",
                "     w  ",
                "r r r r ",
                " r r r r",
                "r r r r "));

    Player firstPlayer = new Player("first");
    Player secondPlayer = new Player("second");
    Game game = Game.createGameWithBoard(firstPlayer, secondPlayer, board);

    Position firstPosition = new Position(5, 6);
    Position secondPosition = new Position(3, 4);
    Position thirdPosition = new Position(1, 2);

    Move firstMove = new Move(firstPosition, secondPosition);
    Move secondMove = new Move(secondPosition, thirdPosition);

    SessionWrapper session = new MockSessionWrapper(firstPlayer);

    {
      Message message = route.handleJson(session, game, firstMove, null);
      assertEquals(Type.info, message.getType());
    }

    {
      Message message = route.handleJson(session, game, secondMove, null);
      assertEquals(Type.info, message.getType());
    }
  }
}
