package com.webcheckers.ui;

import static com.webcheckers.Maps.entry;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.webcheckers.appl.PlayerLobby;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import spark.ModelAndView;
import spark.RequestResponseFactory;

@Tag("UI-tier")
class GetSignInRouteTest {

  @Test
  void checkLobby() {
    PlayerLobby lobby = new PlayerLobby();

    GetSignInRoute route = new GetSignInRoute(lobby);

    MockHttpServletResponse response = new MockHttpServletResponse();
    MockHttpServletRequest request = new MockHttpServletRequest();

    Map<String, Object> expectedViewModel =
        Stream.of(entry("playerList", lobby))
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

    ModelAndView modelAndView =
        route.handle(
            RequestResponseFactory.create(request), RequestResponseFactory.create(response));

    assertAll(
        () -> assertEquals("signin.ftl", modelAndView.getViewName()),
        () -> assertEquals(expectedViewModel, modelAndView.getModel()));
  }
}
