package com.webcheckers.ui;

import com.google.gson.Gson;
import com.webcheckers.appl.GameCenter;
import com.webcheckers.appl.PlayerLobby;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import spark.Service;
import spark.TemplateEngine;
import spark.template.freemarker.FreeMarkerEngine;

@Tag("UI-tier")
class WebServerTest {

  /** A smoke test to make sure the web server can boot. */
  @Test
  void crashTest() {
    TemplateEngine templateEngine = new FreeMarkerEngine();
    Gson gson = new Gson();
    PlayerLobby lobby = new PlayerLobby();
    Service service = Service.ignite();
    GameCenter gameCenter = new GameCenter();

    WebServer server = new WebServer(service, templateEngine, gson, lobby, gameCenter);
    server.initialize();

    service.stop();
  }
}
