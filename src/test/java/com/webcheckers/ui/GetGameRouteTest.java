package com.webcheckers.ui;

import static com.webcheckers.Maps.entry;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import com.webcheckers.model.view.GameViewModel.ViewMode;
import com.webcheckers.model.view.Piece.Color;
import com.webcheckers.ui.session.MockSessionWrapper;
import com.webcheckers.ui.session.SessionWrapper;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.stream.Stream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import spark.ModelAndView;
import spark.RequestResponseFactory;
import spark.Response;

@Tag("UI-tier")
class GetGameRouteTest {

  /** A dependency of the route under test. */
  private final GameCenter gameCenter = new GameCenter();

  /** The route under test. */
  private final GetGameRoute route = new GetGameRoute(gameCenter);

  private final MockHttpServletResponse response = new MockHttpServletResponse();
  private final Response sparkResponse = RequestResponseFactory.create(response);

  @Test
  void validInitialGame() throws UnsupportedEncodingException {
    Player firstPlayer = new Player("first player");
    Player secondPlayer = new Player("second player");
    Game game = Game.createGame(firstPlayer, secondPlayer);

    SessionWrapper session = new MockSessionWrapper(firstPlayer);

    ModelAndView modelAndView = route.handle(session, game, sparkResponse);

    assertEquals("game.ftl", modelAndView.getViewName());

    // Check Some Values
    @SuppressWarnings("unchecked")
    Map<String, Object> vm = (Map<String, Object>) modelAndView.getModel();
    Stream.of(
            entry("currentPlayer", firstPlayer),
            entry("viewMode", ViewMode.PLAY),
            entry("redPlayer", firstPlayer),
            entry("whitePlayer", secondPlayer),
            entry("activeColor", Color.RED),
            entry("title", "Game"))
        .forEach(
            container -> {
              String key = container.getKey();
              Object expectedValue = container.getValue();
              Object actualValue = vm.get(key);
              assertEquals(expectedValue, actualValue);
            });
  }
}
