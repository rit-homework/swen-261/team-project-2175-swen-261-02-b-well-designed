package com.webcheckers.ui;

import static com.webcheckers.Maps.entry;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.webcheckers.appl.PlayerLobby;
import com.webcheckers.model.Player;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import spark.ModelAndView;
import spark.RequestResponseFactory;

@Tag("UI-tier")
class PostSignInRouteTest {

  @Test
  void failsInvalidName() {
    PlayerLobby lobby = new PlayerLobby();

    MockHttpServletRequest request = new MockHttpServletRequest();
    request.addParameter("name", "#$#Q#!");
    MockHttpServletResponse response = new MockHttpServletResponse();

    PostSignInRoute route = new PostSignInRoute(lobby);
    ModelAndView modelAndView =
        route.handle(
            RequestResponseFactory.create(request), RequestResponseFactory.create(response));

    Map<String, Object> expectedViewModel =
        Stream.of(entry("message", "That username isn't allowed."))
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

    assertAll(
        () -> assertEquals("signin.ftl", modelAndView.getViewName()),
        () -> assertEquals(expectedViewModel, modelAndView.getModel()));
  }

  @Test
  void failsExistingPlayer() {
    PlayerLobby lobby = new PlayerLobby();
    Player existingPlayer = new Player("existing player");
    lobby.signIn(existingPlayer);

    MockHttpServletRequest request = new MockHttpServletRequest();
    request.addParameter("name", existingPlayer.getName());

    MockHttpServletResponse response = new MockHttpServletResponse();

    PostSignInRoute route = new PostSignInRoute(lobby);
    ModelAndView modelAndView =
        route.handle(
            RequestResponseFactory.create(request), RequestResponseFactory.create(response));

    Map<String, Object> expectedViewModel =
        Stream.of(entry("message", "That username isn't allowed."))
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

    assertAll(
        () -> assertEquals("signin.ftl", modelAndView.getViewName()),
        () -> assertEquals(expectedViewModel, modelAndView.getModel()));
  }

  @Test
  void allowedUserName() {
    PlayerLobby lobby = new PlayerLobby();
    String playerName = "valid player";

    MockHttpServletRequest request = new MockHttpServletRequest();
    request.addParameter("name", playerName);

    MockHttpServletResponse response = new MockHttpServletResponse();

    PostSignInRoute route = new PostSignInRoute(lobby);
    ModelAndView modelAndView =
        route.handle(
            RequestResponseFactory.create(request), RequestResponseFactory.create(response));

    assertNull(modelAndView);

    Player player = lobby.getPlayer(playerName);
    assertEquals(playerName, player.getName());
  }
}
