package com.webcheckers.ui;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Game;
import com.webcheckers.model.Move;
import com.webcheckers.model.Player;
import com.webcheckers.model.PlayerGameState;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Message;
import com.webcheckers.model.view.Message.Type;
import com.webcheckers.ui.session.MockSessionWrapper;
import com.webcheckers.ui.session.SessionWrapper;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("UI-tier")
class BackupMoveRouteTest {

  private final GameCenter gameCenter = new GameCenter();
  private final BackupMoveRoute route = new BackupMoveRoute(gameCenter);

  @Test
  void notYourTurn() {
    Player player1 = new Player("player1");
    Player player2 = new Player("player2");
    Game game = Game.createGame(player1, player2);
    SessionWrapper session = new MockSessionWrapper(player2);

    Message message = route.handle(session, game);
    assertAll(
        () -> assertEquals("It is not your turn yet.", message.getText()),
        () -> assertEquals(Type.error, message.getType()));
  }

  @Test
  void noValidatedMove() {
    Player player1 = new Player("player1");
    Player player2 = new Player("player2");
    Game game = Game.createGame(player1, player2);
    SessionWrapper session = new MockSessionWrapper(player1);

    Message message = route.handle(session, game);
    assertAll(
        () -> assertEquals("No move has been proposed.", message.getText()),
        () -> assertEquals(Type.error, message.getType()));
  }

  @Test
  void correctSubmit() {
    Player player1 = new Player("player1");
    Player player2 = new Player("player2");
    Game game = Game.createGame(player1, player2);
    SessionWrapper session = new MockSessionWrapper(player1);

    Position startPosition = new Position(5, 2);
    Position endPosition = new Position(4, 3);
    Move move = new Move(startPosition, endPosition);

    PlayerGameState state = game.getStateForPlayer(player1);
    state.pushLastValidatedMove(move);
    game.getBoard().applyMove(move);

    Message message = route.handle(session, game);
    assertAll(
        () -> assertEquals("Move undone", message.getText()),
        () -> assertEquals(Type.info, message.getType()));
  }
}
