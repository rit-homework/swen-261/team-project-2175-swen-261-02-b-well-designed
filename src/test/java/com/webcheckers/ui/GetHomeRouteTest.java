package com.webcheckers.ui;

import static com.webcheckers.Maps.entry;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.appl.PlayerLobby;
import com.webcheckers.model.Player;
import com.webcheckers.ui.session.MockSessionWrapper;
import com.webcheckers.ui.session.SessionWrapper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import spark.ModelAndView;

@Tag("UI-tier")
class GetHomeRouteTest {

  private final GameCenter gameCenter = new GameCenter();
  private final PlayerLobby playerLobby = new PlayerLobby();
  private final GetHomeRoute route = new GetHomeRoute(playerLobby, gameCenter);

  @Test
  void testSignedOut() {
    Player player1 = new Player("player 1");
    Player player2 = new Player("player 2");

    playerLobby.signIn(player1);
    playerLobby.signIn(player2);

    SessionWrapper session = new MockSessionWrapper(null);

    ModelAndView modelAndView = route.handle("", session);

    Map<String, Object> expectedViewModel =
        Stream.of(entry("title", "Welcome!"), entry("numPlayers", 2), entry("message", ""))
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

    assertAll(
        () -> assertEquals("home.ftl", modelAndView.getViewName()),
        () -> assertEquals(expectedViewModel, modelAndView.getModel()));
  }

  @Test
  void testSignedIn() {
    Player player1 = new Player("player 1");
    Player player2 = new Player("player 2");

    playerLobby.signIn(player1);
    playerLobby.signIn(player2);

    SessionWrapper session = new MockSessionWrapper(player1);

    ModelAndView modelAndView = route.handle("", session);

    Map<String, Object> expectedViewModel =
        Stream.of(
                entry("title", "Welcome!"),
                entry("numPlayers", 2),
                entry("message", ""),
                entry("playerList", Collections.singletonList("player 2")),
                entry("displayName", "player 1"),
                entry("activeGameList", new ArrayList<>()),
                entry("inactiveGameList", new ArrayList<>()))
            .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

    assertAll(
        () -> assertEquals("home.ftl", modelAndView.getViewName()),
        () -> assertEquals(expectedViewModel, modelAndView.getModel()));
  }
}
