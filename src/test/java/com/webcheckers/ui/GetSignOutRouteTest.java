package com.webcheckers.ui;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.appl.PlayerLobby;
import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import com.webcheckers.ui.session.MockSessionWrapper;
import com.webcheckers.ui.session.SessionWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import spark.ModelAndView;
import spark.RequestResponseFactory;
import spark.Response;

class GetSignOutRouteTest {

  private final GameCenter gameCenter = new GameCenter();
  private final PlayerLobby lobby = new PlayerLobby();
  private final GetSignOutRoute route = new GetSignOutRoute(lobby);
  private final MockHttpServletResponse response = new MockHttpServletResponse();
  private final Response sparkResponse = RequestResponseFactory.create(response);

  @Test
  void signedInWithGame() {
    Player first = new Player("first");
    Player second = new Player("second");
    String key = gameCenter.createGame(first, second);
    Game game = gameCenter.getGame(key);

    SessionWrapper session = new MockSessionWrapper(first);

    ModelAndView modelAndView = route.handle(session, sparkResponse);

    assertAll(
        () -> assertNull(modelAndView), () -> assertEquals("/signin", response.getRedirectedUrl()));

    assertNull(lobby.getPlayer("player"));
    assertTrue(game.isActive());
  }
}
