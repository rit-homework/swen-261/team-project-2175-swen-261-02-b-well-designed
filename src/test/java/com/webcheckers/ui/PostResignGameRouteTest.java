package com.webcheckers.ui;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Game;
import com.webcheckers.model.Move;
import com.webcheckers.model.Player;
import com.webcheckers.model.PlayerGameState;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Message;
import com.webcheckers.model.view.Message.Type;
import com.webcheckers.ui.session.MockSessionWrapper;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

@Tag("UI-tier")
class PostResignGameRouteTest {

  private final GameCenter gameCenter = new GameCenter();
  private final PostResignGameRoute route = new PostResignGameRoute(gameCenter);

  @Test
  void notTurn() {
    Player player1 = new Player("Player1");
    Player player2 = new Player("Player2");

    MockSessionWrapper session = new MockSessionWrapper(player1);
    session.setPlayer(player2);
    Game game = Game.createGame(player1, player2);

    Message message = route.handle(session, game);

    assertAll(
        () ->
            assertEquals("It is not your turn. Wait until your turn to resign.", message.getText()),
        () -> assertEquals(Type.error, message.getType()));
  }

  @Test
  void submittedMove() {
    Player player1 = new Player("Player1");
    Player player2 = new Player("Player2");

    MockSessionWrapper session = new MockSessionWrapper(player1);
    session.setPlayer(player1);

    Position startPosition = new Position(5, 2);
    Position endPosition = new Position(4, 3);
    Move move = new Move(startPosition, endPosition);

    Game game = Game.createGame(player1, player2);
    PlayerGameState state = game.getStateForPlayer(player1);
    state.pushLastValidatedMove(move);

    Message message = route.handle(session, game);

    assertAll(
        () ->
            assertEquals("Moves have been proposed. Abort the move to resign.", message.getText()),
        () -> assertEquals(Type.error, message.getType()));
  }

  @Test
  void handleValidResignation() {
    Player player1 = new Player("Player1");
    Player player2 = new Player("Player2");

    MockSessionWrapper session = new MockSessionWrapper(player1);
    session.setPlayer(player1);

    Game game = Game.createGame(player1, player2);

    Message message = route.handle(session, game);
    assertAll(
        () -> assertEquals("Resigned successfully.", message.getText()),
        () -> assertEquals(Type.info, message.getType()));
  }
}
