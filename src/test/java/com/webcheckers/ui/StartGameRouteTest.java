package com.webcheckers.ui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.appl.PlayerLobby;
import com.webcheckers.model.Player;
import com.webcheckers.ui.session.MockSessionWrapper;
import com.webcheckers.ui.session.SessionWrapper;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import spark.ModelAndView;
import spark.RequestResponseFactory;
import spark.Response;

@Tag("UI-tier")
class StartGameRouteTest {
  private final GameCenter gameCenter = new GameCenter();
  private final PlayerLobby lobby = new PlayerLobby();
  private final StartGameRoute route = new StartGameRoute(lobby, gameCenter);
  private final MockHttpServletResponse response = new MockHttpServletResponse();
  private final Response sparkResponse = RequestResponseFactory.create(response);

  @Test
  void notSignedIn() {
    Player otherPlayer = new Player("other player");
    lobby.signIn(otherPlayer);

    SessionWrapper session = new MockSessionWrapper(null);

    ModelAndView modelAndView = route.handle(session, otherPlayer.getName(), sparkResponse);
    assertNull(modelAndView);

    assertEquals("/signin", response.getRedirectedUrl());
  }

  @Test
  void otherPlayerMissing() {
    Player player1 = new Player("player 1");
    lobby.signIn(player1);

    SessionWrapper session = new MockSessionWrapper(player1);

    ModelAndView modelAndView = route.handle(session, null, sparkResponse);
    assertNull(modelAndView);

    assertEquals("/", response.getRedirectedUrl());
  }

  @Test
  void otherPlayerNotExist() {
    Player player1 = new Player("player 1");
    Player otherPlayer = new Player("other player");

    lobby.signIn(player1);

    SessionWrapper session = new MockSessionWrapper(player1);

    ModelAndView modelAndView = route.handle(session, otherPlayer.getName(), sparkResponse);
    assertNull(modelAndView);

    assertEquals("/", response.getRedirectedUrl());
  }

  @Test
  void otherPlayerOccupied() {
    Player player1 = new Player("player 1");
    Player player2 = new Player("player 2");
    String firstGameKey = gameCenter.createGame(player1, player2);

    Player otherPlayer = new Player("other player");

    lobby.signIn(player1);
    lobby.signIn(player2);
    lobby.signIn(otherPlayer);

    SessionWrapper session = new MockSessionWrapper(otherPlayer);

    ModelAndView modelAndView = route.handle(session, player2.getName(), sparkResponse);
    assertNull(modelAndView);

    String newGameKey =
        gameCenter.allGameKeys().filter(key -> !key.equals(firstGameKey)).findFirst().get();
    assertEquals(GetGameRoute.withId(newGameKey), response.getRedirectedUrl());
  }

  @Test
  void valid() {
    Player player1 = new Player("player 1");
    Player player2 = new Player("player 2");

    lobby.signIn(player1);
    lobby.signIn(player2);

    SessionWrapper session = new MockSessionWrapper(player1);

    ModelAndView modelAndView = route.handle(session, player2.getName(), sparkResponse);
    assertNull(modelAndView);

    String newGameKey = gameCenter.allGameKeys().findFirst().get();

    assertEquals(GetGameRoute.withId(newGameKey), response.getRedirectedUrl());
  }
}
