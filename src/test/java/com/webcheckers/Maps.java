package com.webcheckers;

import java.util.AbstractMap;
import java.util.Map;

/** A helper class to make initializing maps easier. */
public class Maps {

  public static <K, V> Map.Entry<K, V> entry(K key, V value) {
    return new AbstractMap.SimpleEntry<>(key, value);
  }
}
