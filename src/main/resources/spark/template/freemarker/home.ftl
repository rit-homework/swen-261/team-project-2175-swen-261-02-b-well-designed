<!DOCTYPE html>
<html>
<head>
  <#setting url_escaping_charset="UTF-8">

  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta http-equiv="refresh" content="10">
  <title>${title} | Web Checkers</title>
  <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>
<div class="page">

  <h1>Web Checkers</h1>

  <div class="navigation">
    <a href="/">my home</a>
    <a href="/signin">sign in</a>
  </div>

  <div class="body">
    <#if message??><p class="error">${message}</p></#if>

    <#if displayName??><p>Hello ${displayName}!</p></#if>

    <p>Welcome to the world of online Checkers.</p>
    <p>There are ${numPlayers} active users on this site.</p>

    <#if (playerList)??>
      <p>Active Players:</p>
      <ul>
          <#list playerList as player>
            <li><a href="/start?player=${player?url}">${player}</a></li>
          </#list>
      </ul>

      <p>Active Games:</p>
      <ul>
            <#list activeGameList as game>
              <li><a href="/${game}/game">${game}</a></li>
            </#list>
      </ul>

      <p>Inactive Games:</p>
      <ul>
            <#list inactiveGameList as game>
              <li><a href="/${game}/game">${game}</a></li>
            </#list>
      </ul>
    </#if>
  </div>

</div>
</body>
</html>
