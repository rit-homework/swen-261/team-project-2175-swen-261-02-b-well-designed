package com.webcheckers.ui;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.appl.PlayerLobby;
import com.webcheckers.model.Player;
import com.webcheckers.ui.session.SessionWrapper;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * The UI Controller to GET the Home page.
 *
 * @author <a href='mailto:bdbvse@rit.edu'>Bryan Basham</a>
 */
public class GetHomeRoute implements TemplateViewRoute {

  private static final Logger LOG = Logger.getLogger(GetHomeRoute.class.getName());

  private static final String DISPLAY_NAME_ATTR = "displayName";

  private final PlayerLobby lobby;

  private final GameCenter gameCenter;

  /** Create the Spark Route (UI controller) for the {@code GET /} HTTP request. */
  GetHomeRoute(PlayerLobby lobby, GameCenter gameCenter) {
    this.lobby = lobby;
    this.gameCenter = gameCenter;
  }

  ModelAndView handle(String message, SessionWrapper session) {
    Collection<Player> players = lobby.getPlayers();

    Map<String, Object> vm = new HashMap<>();
    vm.put("title", "Welcome!");
    vm.put("numPlayers", players.size());

    Player player = session.getPlayer();
    if (player != null) {
      // If there is a session, show the list of players.

      vm.put(
          "activeGameList",
          gameCenter.getGamesWithActivity(player, true).collect(Collectors.toList()));

      vm.put(
          "inactiveGameList",
          gameCenter.getGamesWithActivity(player, false).collect(Collectors.toList()));

      List<String> playerNames =
          players
              .stream()
              .filter(otherPlayer -> !otherPlayer.equals(player))
              .map(Player::getName)
              .collect(Collectors.toList());

      vm.put("playerList", playerNames);
      vm.put(DISPLAY_NAME_ATTR, player.getName());
    }

    vm.put("message", message);

    return new ModelAndView(vm, "home.ftl");
  }

  /**
   * Render the WebCheckers Home page.
   *
   * @param request the HTTP request
   * @param response the HTTP response
   * @return A model and view to render.
   */
  @Override
  public ModelAndView handle(Request request, Response response) {
    String message = request.queryParams("message");
    SessionWrapper session = SessionWrapper.from(request);

    return handle(message, session);
  }
}
