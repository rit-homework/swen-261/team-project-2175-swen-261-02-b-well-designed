package com.webcheckers.ui;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Board;
import com.webcheckers.model.Game;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveType;
import com.webcheckers.model.Player;
import com.webcheckers.model.PlayerGameState;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Message;
import com.webcheckers.model.view.Message.Type;
import com.webcheckers.ui.session.SessionWrapper;
import spark.Request;
import spark.Response;
import spark.Route;

public class SubmitTurnRoute implements Route {

  private final GameCenter gameCenter;

  SubmitTurnRoute(GameCenter gameCenter) {
    this.gameCenter = gameCenter;
  }

  public Message handle(SessionWrapper session, Game game) {
    // Check If Turn
    Player player = session.getPlayer();
    Message message = CheckTurnRoute.ensureActivePlayer(player, game);
    if (message != null) {
      return message;
    }

    PlayerGameState state = game.getStateForPlayer(player);
    if (state.isLastValidatedMoveStackEmpty()) {
      return new Message("No moves have been proposed.", Type.error);
    }

    Board gameBoard = game.getBoard();

    Move lastValidatedMove = state.peekLastValidatedMove();
    Position moveEndPosition = lastValidatedMove.getEnd();
    MoveType lastMoveType = lastValidatedMove.getMoveType();
    if (lastMoveType == MoveType.JUMP && gameBoard.pieceCanJump(moveEndPosition)) {
      return new Message("There are more moves to complete the jump move.", Type.error);
    }

    // King Pieces After Submission
    game.kingPieces(player);

    Player otherPlayer = game.getOtherPlayer(player);
    if (!game.playerCanMove(otherPlayer)) {
      // The Other Player Can't Move, They Loose
      game.endGameWithWinner(player);
    }

    game.nextTurn();
    state.emptyMoveStack();

    return new Message("Move Submitted", Message.Type.info);
  }

  @Override
  public Message handle(Request request, Response response) {
    SessionWrapper session = SessionWrapper.from(request);
    Game game = RouteParamHelper.getGame(request, session, gameCenter, response);
    if (game == null) {
      return null;
    }

    return handle(session, game);
  }
}
