package com.webcheckers.ui;

import com.webcheckers.appl.PlayerLobby;
import com.webcheckers.model.Player;
import com.webcheckers.ui.session.SessionWrapper;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class GetSignOutRoute implements TemplateViewRoute {
  private final PlayerLobby lobby;

  GetSignOutRoute(PlayerLobby playerLobby) {
    this.lobby = playerLobby;
  }

  public ModelAndView handle(SessionWrapper session, Response response) {
    Player player = session.getPlayer();
    if (player != null) {
      lobby.signOut(player);
      session.invalidateSession();
    }

    response.redirect("/signin");
    return null;
  }

  @Override
  public ModelAndView handle(Request request, Response response) {
    SessionWrapper session = SessionWrapper.from(request);
    return handle(session, response);
  }
}
