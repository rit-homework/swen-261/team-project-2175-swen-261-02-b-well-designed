package com.webcheckers.ui;

import com.google.gson.Gson;
import com.webcheckers.appl.GameCenter;
import com.webcheckers.appl.PlayerLobby;
import java.util.Objects;
import java.util.logging.Logger;
import spark.Service;
import spark.TemplateEngine;

/**
 * The server that initializes the set of HTTP request handlers. This defines the <em>web
 * application interface</em> for this WebCheckers application.
 *
 * <p>There are multiple ways in which you can have the client issue a request and the application
 * generate responses to requests. If your team is not careful when designing your approach, you can
 * quickly create a mess where no one can remember how a particular request is issued or the
 * response gets generated. Aim for consistency in your approach for similar activities or requests.
 *
 * <p>Design choices for how the client makes a request include:
 *
 * <ul>
 *   <li>Request URL
 *   <li>HTTP verb for request (GET, POST, PUT, DELETE and so on)
 *   <li><em>Optional:</em> Inclusion of request parameters
 * </ul>
 *
 * <p>Design choices for generating a response to a request include:
 *
 * <ul>
 *   <li>View templates with conditional elements
 *   <li>Use different view templates based on results of executing the client request
 *   <li>Redirecting to a different application URL
 * </ul>
 *
 * @author <a href='mailto:bdbvse@rit.edu'>Bryan Basham</a>
 */
public class WebServer {

  private static final Logger LOG = Logger.getLogger(WebServer.class.getName());

  private final JSONTransformer transformer;
  private final TemplateEngine templateEngine;
  private final Gson gson;
  private final PlayerLobby lobby;
  private final Service service;
  private final GameCenter gameCenter;

  public WebServer(
      final Service service,
      final TemplateEngine templateEngine,
      final Gson gson,
      PlayerLobby lobby,
      GameCenter gameCenter) {
    Objects.requireNonNull(service);
    Objects.requireNonNull(templateEngine, "templateEngine must not be null");
    Objects.requireNonNull(gson, "gson must not be null");
    Objects.requireNonNull(lobby, "lobby must not be null");

    this.service = service;
    this.templateEngine = templateEngine;
    this.gson = gson;
    this.lobby = lobby;
    this.transformer = new JSONTransformer(gson);
    this.gameCenter = gameCenter;
  }

  /** Initializes the and spark service which is responsible for serving the web checkers site. */
  public void initialize() {
    // Configuration to serve static files
    service.staticFileLocation("/public");

    //// Setting any route (or filter) in Spark triggers initialization of the
    //// embedded Jetty web server.

    //// A route is set for a request verb by specifying the path for the
    //// request, and the function callback (request, response) -> {} to
    //// process the request. The order that the routes are defined is
    //// important. The first route (request-path combination) that matches
    //// is the one which is invoked. Additional documentation is at
    //// http://sparkjava.com/documentation.html and in Spark tutorials.

    //// Each route (processing function) will check if the request is valid
    //// from the client that made the request. If it is valid, the route
    //// will extract the relevant data from the request and pass it to the
    //// application object delegated with executing the request. When the
    //// delegate completes execution of the request, the route will create
    //// the parameter map that the response template needs. The data will
    //// either be in the value the delegate returns to the route after
    //// executing the request, or the route will query other application
    //// objects for the data needed.

    //// FreeMarker defines the HTML response using templates. Additional
    //// documentation is at
    //// http://freemarker.org/docs/dgui_quickstart_template.html.
    //// The Spark FreeMarkerEngine lets you pass variable values to the
    //// template via a map. Additional information is in online
    //// tutorials such as
    //// http://benjamindparrish.azurewebsites.net/adding-freemarker-to-java-spark/.

    //// These route definitions are examples. You will define the routes
    //// that are appropriate for the HTTP client interface that you define.
    //// Create separate Route classes to handle each route; this keeps your
    //// code clean; using small classes.

    // Register Logging Middleware
    service.before(
        ((request, response) -> {
          String path = request.pathInfo();
          String method = request.requestMethod();

          LOG.finer(String.format("[%s] %s", method, path));
        }));

    // Shows the Checkers game Home page.
    service.get("/", new GetHomeRoute(lobby, gameCenter), templateEngine);
    service.get("/signin", new GetSignInRoute(lobby), templateEngine);
    service.post("/signin", new PostSignInRoute(lobby), templateEngine);

    service.get(GetGameRoute.withId(":gameId"), new GetGameRoute(gameCenter), templateEngine);
    service.get("/start", new StartGameRoute(lobby, gameCenter), templateEngine);

    service.post(
        "/:gameId/validateMove",
        "application/json",
        new ValidateMoveRoute(gson, gameCenter),
        transformer);
    service.post("/:gameId/submitTurn", new SubmitTurnRoute(gameCenter), transformer);
    service.post("/:gameId/checkTurn", new CheckTurnRoute(gameCenter), transformer);
    service.post("/:gameId/backupMove", new BackupMoveRoute(gameCenter), transformer);
    service.post("/:gameId/resignGame", new PostResignGameRoute(gameCenter), transformer);

    service.get("/signout", new GetSignOutRoute(lobby), templateEngine);

    LOG.config("WebServer is initialized.");
  }
}
