package com.webcheckers.ui;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.appl.PlayerLobby;
import com.webcheckers.model.Player;
import com.webcheckers.ui.session.SessionWrapper;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/** Handler for GET /start */
public class StartGameRoute implements TemplateViewRoute {

  private final PlayerLobby lobby;

  private final GameCenter gameCenter;

  StartGameRoute(PlayerLobby lobby, GameCenter gameCenter) {
    this.lobby = lobby;
    this.gameCenter = gameCenter;
  }

  public ModelAndView handle(SessionWrapper session, String otherPlayerName, Response response) {
    // Get Current Player
    Player currentPlayer = session.getPlayer();
    if (currentPlayer == null) {
      // Invalid Current Player, Send Them To Sign In Page
      response.redirect("/signin");
      return null;
    }

    // Check Other Player
    if (otherPlayerName == null) {
      // No other player was specified. Redirect to home.
      response.redirect("/");
      return null;
    }

    Player otherPlayer = lobby.getPlayer(otherPlayerName);
    // Ensure Other Player Exists
    if (otherPlayer == null) {
      response.redirect("/");
      return null;
    }

    String identifier = gameCenter.createGame(currentPlayer, otherPlayer);
    response.redirect(GetGameRoute.withId(identifier));
    return null;
  }

  @Override
  public ModelAndView handle(Request request, Response response) {
    SessionWrapper session = SessionWrapper.from(request);
    String otherPlayerName = request.queryParams("player");

    return handle(session, otherPlayerName, response);
  }
}
