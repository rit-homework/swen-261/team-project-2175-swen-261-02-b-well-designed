package com.webcheckers.ui;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import com.webcheckers.ui.session.SessionWrapper;
import spark.Request;
import spark.Response;

/** A helper class for checking pre-conditions and getting a game for a route. */
public class RouteParamHelper {

  /**
   * Get game id parameter from request.
   *
   * @param request A spark request.
   * @return The game id associated with the request.
   */
  private static String getGameId(Request request) {
    return request.params("gameId");
  }

  /**
   * Ensures that a game id is provided, a game with the game id exists and the player is a part of
   * the game.
   *
   * @param request The request to get the game id from.
   * @param session The session from which to get the player.
   * @param gameCenter The game center to retrieve games from.
   * @param response The response to write failure messages to.
   * @return A game, or null if a game couldn't be gotten.
   */
  public static Game getGame(
      Request request, SessionWrapper session, GameCenter gameCenter, Response response) {
    String gameId = getGameId(request);
    if (gameId == null) {
      response.status(404);
      response.body("No Game ID Was Provided");
      return null;
    }

    Game game = gameCenter.getGame(gameId);
    if (game == null) {
      response.status(404);
      response.body("No Game With That ID Was Found");
      return null;
    }

    Player player = session.getPlayer();
    if (player == null) {
      response.redirect("/signIn");
      return null;
    }

    // Check If Player In Game
    if (!game.playerInGame(player)) {
      response.status(403);
      response.body("You aren't allowed to see that game.");
      return null;
    }

    return game;
  }
}
