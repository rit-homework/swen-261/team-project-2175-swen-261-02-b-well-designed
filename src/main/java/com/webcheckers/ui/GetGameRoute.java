package com.webcheckers.ui;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import com.webcheckers.model.view.GameViewModel;
import com.webcheckers.ui.session.SessionWrapper;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/** Handler for the GET /game route. */
public class GetGameRoute implements TemplateViewRoute {

  private static final Logger LOG = Logger.getLogger(GetHomeRoute.class.getName());

  private static final String TITLE_ATTR = "title";

  private final GameCenter gameCenter;

  GetGameRoute(GameCenter gameCenter) {
    this.gameCenter = gameCenter;
    LOG.config("GetGameRoute is initialized.");
  }

  /**
   * The path to this route with parameters filled in.
   *
   * @param identifier The value to insert for the gameId parameter.
   * @return A string with the path of the route with parameters filled in.
   */
  static String withId(String identifier) {
    return String.format("/%s/game", identifier);
  }

  /**
   * The handler for the /game route.
   *
   * @param session The session associated with the request. This is used to determine the player.
   *     This player should be participating in the game.
   * @param game The game which will be displayed.
   * @param response The response.
   * @return The rendered result shown to the user.
   */
  ModelAndView handle(SessionWrapper session, Game game, Response response)
      throws UnsupportedEncodingException {
    Player player = session.getPlayer();
    Map<String, Object> vm = new HashMap<>();
    vm.put(TITLE_ATTR, "Game");

    if (!game.isActive()) {
      String message =
          URLEncoder.encode(game.getLoser().equals(player) ? "You Lost" : "You Win!", "UTF-8");
      response.redirect(String.format("/?message=%s", message));
      return null;
    }

    GameViewModel.fromGame(player, game).makeViewModel().forEach(vm::put);

    return new ModelAndView(vm, "game.ftl");
  }

  @Override
  public ModelAndView handle(Request request, Response response)
      throws UnsupportedEncodingException {
    SessionWrapper session = SessionWrapper.from(request);
    Game game = RouteParamHelper.getGame(request, session, gameCenter, response);
    if (game == null) {
      return null;
    }

    return handle(session, game, response);
  }
}
