package com.webcheckers.ui;

import com.webcheckers.appl.PlayerLobby;
import java.util.HashMap;
import java.util.Map;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/** Handler for the GET /sign route. */
public class GetSignInRoute implements TemplateViewRoute {

  private final PlayerLobby lobby;

  GetSignInRoute(PlayerLobby playerLobby) {
    this.lobby = playerLobby;
  }

  @Override
  public ModelAndView handle(Request request, Response response) {
    Map<String, Object> vm = new HashMap<>();
    vm.put("playerList", lobby);
    return new ModelAndView(vm, "signin.ftl");
  }
}
