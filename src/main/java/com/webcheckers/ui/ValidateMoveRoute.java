package com.webcheckers.ui;

import com.google.gson.Gson;
import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Board;
import com.webcheckers.model.Game;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.MoveType;
import com.webcheckers.model.Player;
import com.webcheckers.model.PlayerGameState;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Message;
import com.webcheckers.model.view.Message.Type;
import com.webcheckers.model.view.Piece.Color;
import com.webcheckers.ui.session.SessionWrapper;
import spark.Request;
import spark.Response;

class ValidateMoveRoute extends JSONRoute<Move> {

  private final GameCenter gameCenter;

  ValidateMoveRoute(Gson gson, GameCenter gameCenter) {
    super(gson, Move.class);
    this.gameCenter = gameCenter;
  }

  Message handleJson(SessionWrapper session, Game game, Move move, Response response) {
    // Check If Turn
    Player player = session.getPlayer();
    Message message = CheckTurnRoute.ensureActivePlayer(player, game);
    if (message != null) {
      return message;
    }

    Board gameBoard = game.getBoard();

    // Check For Valid Move
    if (!move.containsRequiredFields()) {
      response.status(400);
      return new Message(
          "The input sent to the server is invalid. A required field is null.", Type.error);
    }

    Color playerColor = game.getColor(player);
    move = move.fromPlayerPerspective(playerColor);

    PlayerGameState state = game.getStateForPlayer(player);
    if (!state.isLastValidatedMoveStackEmpty()) {
      Move lastMove = state.peekLastValidatedMove();

      // Check That Past Move Was Jump Move
      if (lastMove.getMoveType() != MoveType.JUMP) {
        return new Message(
            "The last move wasn't a jump move. Can't make a jump move after a simple move.",
            Type.error);
      }

      // Check That Move Continues Jump Move
      Position lastMoveEnd = lastMove.getEnd();
      if (!lastMoveEnd.equals(move.getStart())) {
        return new Message("Only the piece moved in the last move can be moved.", Type.error);
      }

      // Check That Move Is A Jump Move
      if (move.getMoveType() != MoveType.JUMP) {
        return new Message(
            "This move isn't a jump move. A multi-jump move can only be continued with a jump move.",
            Type.error);
      }
    }

    MoveInvalidReason reason = game.validateMove(move, player);
    if (reason != null) {
      return new Message(reason.getReasonSentence(), Type.error);
    }

    gameBoard.applyMove(move);
    state.pushLastValidatedMove(move);

    return new Message("That move is OK!", Type.info);
  }

  @Override
  Message handleJson(Request request, Move move, Response response) {
    SessionWrapper session = SessionWrapper.from(request);
    Game game = RouteParamHelper.getGame(request, session, gameCenter, response);
    if (game == null) {
      return null;
    }

    return handleJson(session, game, move, response);
  }
}
