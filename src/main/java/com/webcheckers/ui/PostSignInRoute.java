package com.webcheckers.ui;

import com.webcheckers.appl.PlayerLobby;
import com.webcheckers.model.Player;
import com.webcheckers.ui.session.SessionWrapper;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/** Handler for POST /signin */
public class PostSignInRoute implements TemplateViewRoute {

  private static final Logger LOG = Logger.getLogger(PostSignInRoute.class.getName());

  private final PlayerLobby lobby;

  PostSignInRoute(PlayerLobby lobby) {
    this.lobby = lobby;
  }

  @Override
  public ModelAndView handle(Request request, Response response) {
    String name = request.queryParams("name");

    LOG.finer(String.format("player name: %s", name));
    if (!lobby.canAddPlayer(name)) {
      LOG.finer(String.format("invalid player name %s", name));

      Map<String, Object> model = new HashMap<>();
      model.put("message", "That username isn't allowed.");

      return new ModelAndView(model, "signin.ftl");
    }

    Player player = new Player(name);
    LOG.finer(String.format("creating session for player %s", name));

    SessionWrapper.from(request).setPlayer(player);
    lobby.signIn(player);

    response.redirect("/");
    return null;
  }
}
