package com.webcheckers.ui;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import com.webcheckers.model.view.Message;
import com.webcheckers.model.view.Message.Type;
import com.webcheckers.ui.session.SessionWrapper;
import spark.Request;
import spark.Response;
import spark.Route;

public class CheckTurnRoute implements Route {

  private final GameCenter gameCenter;

  CheckTurnRoute(GameCenter gameCenter) {
    this.gameCenter = gameCenter;
  }

  static Message ensureActivePlayer(Player player, Game game) {
    if (game.isActivePlayer(player)) {
      return null;
    }

    return new Message("It is not your turn yet.", Type.error);
  }

  @Override
  public Message handle(Request request, Response response) {
    SessionWrapper session = SessionWrapper.from(request);
    Game game = RouteParamHelper.getGame(request, session, gameCenter, response);
    if (game == null) {
      return null;
    }

    return handle(session, game);
  }

  public Message handle(SessionWrapper session, Game game) {
    Player player = session.getPlayer();
    boolean currentPlayerActive = game.isActivePlayer(player);
    return new Message(String.valueOf(currentPlayerActive), Type.info);
  }
}
