package com.webcheckers.ui;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import com.webcheckers.model.PlayerGameState;
import com.webcheckers.model.view.Message;
import com.webcheckers.model.view.Message.Type;
import com.webcheckers.ui.session.SessionWrapper;
import spark.Request;
import spark.Response;
import spark.Route;

public class PostResignGameRoute implements Route {

  private final GameCenter gameCenter;

  PostResignGameRoute(GameCenter gameCenter) {
    this.gameCenter = gameCenter;
  }

  public Message handle(SessionWrapper session, Game game) {
    Player player = session.getPlayer();
    if (!game.isActivePlayer(player)) {
      return new Message("It is not your turn. Wait until your turn to resign.", Type.error);
    }

    PlayerGameState state = game.getStateForPlayer(player);
    if (!state.isLastValidatedMoveStackEmpty()) {
      return new Message("Moves have been proposed. Abort the move to resign.", Type.error);
    }

    game.endGameWithLoser(player);

    // Switch Turn to Force UI to Update
    game.nextTurn();

    return new Message("Resigned successfully.", Type.info);
  }

  @Override
  public Message handle(Request request, Response response) {
    SessionWrapper session = SessionWrapper.from(request);
    Game game = RouteParamHelper.getGame(request, session, gameCenter, response);
    if (game == null) {
      return null;
    }
    return handle(session, game);
  }
}
