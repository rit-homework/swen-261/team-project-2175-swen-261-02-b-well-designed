package com.webcheckers.ui;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/** A response transformer which turns a java object into a JSON string using GSON. */
public class JSONTransformer implements ResponseTransformer {

  /** The GSON instance used to convert the object to json. */
  private final Gson gson;

  JSONTransformer(Gson gson) {
    this.gson = gson;
  }

  @Override
  public String render(Object model) {
    return gson.toJson(model);
  }
}
