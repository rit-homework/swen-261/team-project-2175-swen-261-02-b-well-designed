package com.webcheckers.ui;

import static com.webcheckers.model.view.Message.Type;

import com.webcheckers.appl.GameCenter;
import com.webcheckers.model.Board;
import com.webcheckers.model.Game;
import com.webcheckers.model.Move;
import com.webcheckers.model.Player;
import com.webcheckers.model.PlayerGameState;
import com.webcheckers.model.view.Message;
import com.webcheckers.ui.session.SessionWrapper;
import spark.Request;
import spark.Response;
import spark.Route;

public class BackupMoveRoute implements Route {

  private final GameCenter gameCenter;

  BackupMoveRoute(GameCenter gameCenter) {
    this.gameCenter = gameCenter;
  }

  Message handle(SessionWrapper session, Game game) {
    // Check If Turn
    Player player = session.getPlayer();
    Message message = CheckTurnRoute.ensureActivePlayer(player, game);
    if (message != null) {
      return message;
    }

    // Check validation
    PlayerGameState state = game.getStateForPlayer(player);
    Move lastValidated = state.peekLastValidatedMove();
    if (lastValidated == null) {
      return new Message("No move has been proposed.", Type.error);
    }

    Move lastMove = state.popLastValidatedMove();
    Board gameBoard = game.getBoard();

    gameBoard.undoMove(lastMove);

    return new Message("Move undone", Type.info);
  }

  public Message handle(Request request, Response response) {
    SessionWrapper session = SessionWrapper.from(request);
    Game game = RouteParamHelper.getGame(request, session, gameCenter, response);
    if (game == null) {
      return null;
    }
    return handle(session, game);
  }
}
