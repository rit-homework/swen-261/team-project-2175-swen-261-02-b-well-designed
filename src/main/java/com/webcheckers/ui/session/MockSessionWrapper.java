package com.webcheckers.ui.session;

import com.webcheckers.model.Player;

/** A mock session wrapper which stores session state as object fields. */
public class MockSessionWrapper extends SessionWrapper {

  private Player player;

  public MockSessionWrapper(Player player) {
    this.player = player;
  }

  @Override
  public Player getPlayer() {
    return player;
  }

  @Override
  public void setPlayer(Player player) {
    this.player = player;
  }

  @Override
  public void invalidateSession() {}
}
