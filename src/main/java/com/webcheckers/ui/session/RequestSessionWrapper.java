package com.webcheckers.ui.session;

import com.webcheckers.model.Player;
import spark.Request;

/** A session which stores its state in a {@link Request}. */
public class RequestSessionWrapper extends SessionWrapper {

  private final Request request;

  RequestSessionWrapper(Request request) {
    this.request = request;
  }

  public Player getPlayer() {
    return request.session().attribute("player");
  }

  public void setPlayer(Player player) {
    request.session().attribute("player", player);
  }

  public void invalidateSession() {
    request.session().invalidate();
  }
}
