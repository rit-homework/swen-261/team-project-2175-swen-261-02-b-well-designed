package com.webcheckers.ui.session;

import com.webcheckers.model.Player;
import spark.Request;

/** A type-safe and null-safe wrapper around interactions with the session. */
public abstract class SessionWrapper {

  public static SessionWrapper from(Request request) {
    return new RequestSessionWrapper(request);
  }

  public abstract Player getPlayer();

  public abstract void setPlayer(Player player);

  public abstract void invalidateSession();
}
