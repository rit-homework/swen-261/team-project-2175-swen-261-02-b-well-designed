package com.webcheckers.ui;

import com.google.gson.Gson;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * A helper class for decoding JSON requests.
 *
 * @param <RequestType> The expected type of the incoming request.
 */
public abstract class JSONRoute<RequestType> implements Route {

  /** The GSON instance which will be used to encode and decode JSON. */
  private final Gson gson;

  /** The expected type of the incoming requests. */
  private final Class<RequestType> classOfT;

  JSONRoute(Gson gson, Class<RequestType> classOfT) {
    this.gson = gson;
    this.classOfT = classOfT;
  }

  /**
   * A typed wrapper around JSON interactions. This method is called after the content type has been
   * verified and the body has been decoded.
   *
   * @param request The spark request object.
   * @param thing The parsed JSON request.
   * @param response The spark response object.
   * @return A response object which is to be serialized to JSON.
   */
  abstract Object handleJson(Request request, RequestType thing, Response response)
      throws Exception;

  @Override
  public Object handle(Request request, Response response) throws Exception {
    // Try Parsing JSON
    RequestType thing = gson.fromJson(request.body(), classOfT);
    if (thing == null) {
      response.status(400);

      return "Null Body";
    }

    // Call Handler
    return handleJson(request, thing, response);
  }
}
