package com.webcheckers.appl;

import com.webcheckers.model.Player;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

/** A class to hold the players which are in the lobby. */
public class PlayerLobby {

  /** A map of active player names to their player objects. */
  private final Map<String, Player> players = new TreeMap<>();

  /**
   * Determines if the player can be added to the lobby.
   *
   * @param name The name of the player which is to be added to the lobby.
   * @return true if the user can be created, false if not.
   */
  public boolean canAddPlayer(String name) {
    if (name == null) {
      // The player is null, it can't be added.
      return false;
    }

    if (!Player.isNameValid(name)) {
      // Illegal Characters in Name
      return false;
    }

    // Check Whether Player Exists Already
    return !players.containsKey(name);
  }

  /**
   * Checks whether a player can be added, then adds the player to the list of active players.
   *
   * @param player The player to add.
   */
  public void signIn(Player player) {
    if (!canAddPlayer(player.getName())) {
      throw new IllegalArgumentException("Can't create a player with that name.");
    }

    // Add Player
    players.put(player.getName(), player);
  }

  public void signOut(Player player) {
    players.remove(player.getName());
  }

  /**
   * @return A collection of the all players which have signed in since the beginning of time
   *     (including those in games).
   */
  public Collection<Player> getPlayers() {
    return Collections.unmodifiableCollection(players.values());
  }

  /**
   * Gets a player by name.
   *
   * @param name Gets the player associated with this name.
   * @return The player with the specified name. If there is none, returns null.
   */
  public Player getPlayer(String name) {
    return players.get(name);
  }
}
