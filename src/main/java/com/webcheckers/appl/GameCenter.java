package com.webcheckers.appl;

import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.Stream;

/** A container to hold a list of all games. */
public class GameCenter {

  /** All games ever played. */
  private final Map<String, Game> games = new TreeMap<>();

  /** @return A random string of characters used to identify a game. */
  private static String getRandomIdentifier() {
    return UUID.randomUUID().toString().replace("-", "");
  }

  /**
   * Gets a game with the desired identifier.
   *
   * @param key The key identifying the game.
   * @return The game or null if a game with that identifier doesn't exist.
   */
  public Game getGame(String key) {
    return games.get(key);
  }

  /**
   * Creates a new game with two players.
   *
   * @return The identifier used to get an instance of the game.
   */
  public String createGame(Player firstPlayer, Player secondPlayer) {
    String identifier = getRandomIdentifier();
    Game game = Game.createGame(firstPlayer, secondPlayer);
    games.put(identifier, game);
    return identifier;
  }

  /** @return a stream of all game keys. */
  public Stream<String> allGameKeys() {
    return games.keySet().stream();
  }

  /**
   * A stream of all game keys involving a player.
   *
   * @param player The player to search for games involving.
   * @return A stream of all game containers involving a player.
   */
  private Stream<GameContainer> getGamesInvolving(Player player) {
    return allGameKeys()
        .map(key -> new GameContainer(games.get(key), key))
        .filter(container -> container.game.playerInGame(player));
  }

  /**
   * Gets all keys of games involving a player.
   *
   * @param player the player to find games for.
   * @return A stream of game keys.
   */
  public Stream<String> getGameKeysInvolving(Player player) {
    return getGamesInvolving(player).map(container -> container.key);
  }

  /**
   * Get games with an isActive of active.
   *
   * @param player The player to search for games involving.
   * @param isActive Whether to find active games or inactive ones.
   * @return A stream of all game keys involving a player with activity.
   */
  public Stream<String> getGamesWithActivity(Player player, boolean isActive) {
    return getGamesInvolving(player)
        .filter(container -> container.game.isActive() == isActive)
        .map(container -> container.key);
  }

  private static class GameContainer {
    final Game game;
    final String key;

    private GameContainer(Game game, String key) {
      this.game = game;
      this.key = key;
    }
  }
}
