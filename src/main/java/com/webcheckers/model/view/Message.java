package com.webcheckers.model.view;

/**
 * A container for a message to send to the client.
 *
 * <p>http://www.se.rit.edu/~swen-261/projects/WebCheckers/Sprint1_info.html#message
 */
public class Message {

  private final String text;
  private final Type type;

  public Message(String text, Type type) {
    this.text = text;
    this.type = type;
  }

  public String getText() {
    return text;
  }

  public Type getType() {
    return type;
  }

  public enum Type {
    info,
    error,
  }
}
