package com.webcheckers.model.view;

import com.webcheckers.model.Board;
import com.webcheckers.model.IndexedContainer;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/** A view model for the row. */
public class Row implements Iterable<Space> {

  /** Spaces in the row. */
  private final List<Space> spaces;

  /**
   * The index of the row. This is zero-indexed and is a value from 0 to 7. The zero row is the one
   * furthest away from the player.
   */
  private final int index;

  /**
   * Builds a representation of a row of pieces from an array of pieces.
   *
   * @param row The row from which to initialize the row with.
   * @param rowIdx The index of this row.
   */
  Row(Piece[] row, int rowIdx) {
    // Validate Arguments
    if (row.length != Board.BOARD_COLS) {
      throw new IllegalArgumentException(
          String.format("The size of the input row is incorrect: %d.", row.length));
    }

    if (rowIdx >= Board.BOARD_ROWS) {
      throw new IllegalArgumentException(
          String.format("The index of the row is invalid: %d", rowIdx));
    }

    // Populate Spaces
    spaces =
        IndexedContainer.streamFromArray(row)
            .map(
                container -> {
                  Piece piece = container.value;
                  int colIdx = container.index;

                  return Space.createFromIndex(piece, rowIdx, colIdx);
                })
            .collect(Collectors.toList());

    this.index = rowIdx;
  }

  /** @return An iterator of every space in this row. */
  @Override
  public Iterator<Space> iterator() {
    return this.spaces.iterator();
  }

  public int getIndex() {
    return index;
  }
}
