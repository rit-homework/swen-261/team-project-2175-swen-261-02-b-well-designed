package com.webcheckers.model.view;

import com.webcheckers.model.Board;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/** A container for information to display the board in the game routes. */
public class BoardView implements Iterable<Row> {

  /** A list of rows in the board. The first row is the row furthest away from the player. */
  private List<Row> rows = new ArrayList<>(Board.BOARD_ROWS);

  /**
   * Creates a view of the board from the provided board.
   *
   * @param board The board to create a view of.
   * @param isSecondPlayer Whether or not the second player is looking at the board.
   */
  BoardView(Board board, boolean isSecondPlayer) {
    Piece[][] inputRows = board.getPieces();

    for (int i = 0; i < Board.BOARD_ROWS; i++) {
      rows.add(null);
    }

    if (isSecondPlayer) {
      reverse(inputRows);
    }

    // Loop Over Each Input Row and Create View Row
    for (int i = 0; i < inputRows.length; i++) {
      Piece[] inputRow = inputRows[i];
      if (isSecondPlayer) {
        reverse(inputRow);
      }

      final Row row = new Row(inputRow, i);
      rows.set(i, row);
    }
  }

  /**
   * @return An iterator over the rows of the board. The first row is the furthest from the player.
   */
  @Override
  public Iterator<Row> iterator() {
    return this.rows.iterator();
  }

  /**
   * Reverses an array in place.
   *
   * @param elems The array to reverse.
   * @param <T> The type of each element in the array.
   */
  static <T> void reverse(T[] elems) {
    for (int i = 0; i < elems.length / 2; i++) {
      T temp = elems[i];

      elems[i] = elems[elems.length - i - 1];
      elems[elems.length - i - 1] = temp;
    }
  }
}
