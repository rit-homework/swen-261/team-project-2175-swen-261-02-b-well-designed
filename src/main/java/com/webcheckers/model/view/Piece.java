package com.webcheckers.model.view;

import com.webcheckers.model.Board;

/** Represents a piece on the board. */
public class Piece {

  private final Color color;
  private Type type = Type.SINGLE;

  public Piece(Color color) {
    this.color = color;
  }

  /** @return The color of this piece. */
  public Color getColor() {
    return color;
  }

  /** @return The type of this piece. */
  public Type getType() {
    return type;
  }

  /** Convert this piece to a king. */
  public void makeKing() {
    type = Type.KING;
  }

  /** Convert this piece to a single move piece. */
  public void unKing() {
    type = Type.SINGLE;
  }

  /** The types on the board. */
  public enum Type {
    /** A regular non-king piece. This piece can only move away from the player. */
    SINGLE,

    /** A king piece. This piece can move both away from and towards the player. */
    KING
  }

  /** The colors of pieces on the board. */
  public enum Color {
    /** Pieces of this color belong to the first player. */
    RED,

    /** Pieces of this color belong to the second player. */
    WHITE;

    /** @return The index of the last row that a non-king piece of color can visit. */
    public int getLastRow() {
      if (this == RED) {
        return 0;
      }

      return Board.BOARD_ROWS - 1;
    }

    /** @return The other color. If this is red returns white. Otherwise, returns red. */
    public Color getOther() {
      if (this == RED) {
        return WHITE;
      }

      return RED;
    }
  }

  /**
   * Gets the string representation of the piece. This is not a method on piece because it accepts
   * null values.
   *
   * @param piece The piece to convert to a string.
   * @return The string representation of the piece.
   */
  public static String asString(Piece piece) {
    if (piece == null) {
      return " ";
    }

    if (piece.getColor() == Color.RED) {
      return "r";
    }

    if (piece.getColor() == Color.WHITE) {
      return "w";
    }

    return "!";
  }
}
