package com.webcheckers.model.view;

import com.webcheckers.model.Game;
import com.webcheckers.model.Player;
import com.webcheckers.model.view.Piece.Color;
import freemarker.template.TemplateModel;
import java.util.HashMap;
import java.util.Map;

/** A container for the game's view model. */
public class GameViewModel implements TemplateModel {

  /**
   * The current Player. This is the person viewing this page. In Play mode this must be one of the
   * two players of the game. In other modes it can be any person.
   */
  private final Player currentPlayer;

  /**
   * This is the mode of the Game View. For all of the MVP stories this variable *must* be set to
   * PLAY. For some of the enhancements you may use one of the other mode values. You may also
   * extend this list of modes for other enhancements as appropriate; consult with your instructor
   * about which enhancements could benefit from a new view mode.
   */
  private final ViewMode viewMode = ViewMode.PLAY;

  /** The Player associated with the Red pieces. */
  private final Player redPlayer;

  /** The Player associated with the White pieces. */
  private final Player whitePlayer;

  /** The color of the player whose turn is currently active. */
  private final Color activeColor;

  /** A data type holding the a view of the complete state of the checkers board. */
  private final BoardView board;

  /** An optional data type holding a message from the server to the user. */
  private final Message message;

  private GameViewModel(
      Player currentPlayer,
      Player redPlayer,
      Player whitePlayer,
      Color activeColor,
      BoardView board,
      Message message) {
    this.currentPlayer = currentPlayer;
    this.redPlayer = redPlayer;
    this.whitePlayer = whitePlayer;
    this.activeColor = activeColor;
    this.board = board;
    this.message = message;
  }

  public static GameViewModel fromGame(Player currentPlayer, Game game) {
    Player firstPlayer = game.getFirstPlayer();
    Player secondPlayer = game.getSecondPlayer();
    Player activePlayer = game.getActivePlayer();

    Color activeColor = game.getColor(activePlayer);
    BoardView boardView = new BoardView(game.getBoard(), currentPlayer.equals(secondPlayer));

    return new GameViewModel(
        currentPlayer, firstPlayer, secondPlayer, activeColor, boardView, null);
  }

  /**
   * Makes a stringly typed map of objects which this container holds data about.
   *
   * @return An map representation of the view model.
   */
  public Map<String, Object> makeViewModel() {
    Map<String, Object> vm = new HashMap<>();
    vm.put("currentPlayer", currentPlayer);
    vm.put("viewMode", viewMode);
    vm.put("redPlayer", redPlayer);
    vm.put("whitePlayer", whitePlayer);
    vm.put("activeColor", activeColor);
    vm.put("board", board);
    vm.put("message", message);

    return vm;
  }

  public enum ViewMode {
    PLAY,
    SPECTATOR,
    REPLAY
  }
}
