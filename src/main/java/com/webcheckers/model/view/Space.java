package com.webcheckers.model.view;

/** Holds a representation of a Space in the board. */
public class Space {

  /**
   * The index of the cell in the row. This is a value from 0..7. The zero column is the one
   * furthest to the left side of the board.
   */
  private final int cellIdx;

  /** The piece in the space. If there no piece, this is null. */
  private final Piece piece;

  /** Whether or not this space is black. If it is black, pieces can be placed on it. */
  private final boolean isBlackSquare;

  Space(int cellIdx, Piece piece, boolean isBlackSquare) {
    this.cellIdx = cellIdx;
    this.piece = piece;
    this.isBlackSquare = isBlackSquare;
  }

  public int getCellIdx() {
    return cellIdx;
  }

  /**
   * Checks whether or not this space is black. The coordinates are zero indexed with the origin at
   * the top left corner.
   *
   * @param rowIdx The zero indexed index of the row.
   * @param colIdx The zero indexed index of the colum.
   * @return true if the space at the specified position is black.
   */
  public static boolean isBlackSquare(int rowIdx, int colIdx) {
    return (rowIdx + colIdx) % 2 == 1;
  }

  /**
   * Creates a Space from a piece and the index of the piece.
   *
   * @param piece The piece to put at the space.
   * @param rowIdx The row index.
   * @param colIdx The column index.
   * @return A new space.
   */
  static Space createFromIndex(Piece piece, int rowIdx, int colIdx) {
    boolean isBlackSquare = isBlackSquare(rowIdx, colIdx);
    return new Space(colIdx, piece, isBlackSquare);
  }

  public Piece getPiece() {
    return piece;
  }

  /**
   * @return This method will return true if the space is a valid location to place a piece; that
   *     is, it is a dark square and has no other piece on it.
   */
  public boolean isValid() {
    return isBlackSquare && piece == null;
  }
}
