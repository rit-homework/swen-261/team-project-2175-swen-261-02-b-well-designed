package com.webcheckers.model;

import java.util.Objects;

/** A player entity class. */
public class Player {

  /** The name of the player. */
  private final String name;

  public Player(String name) {
    if (!isNameValid(name)) {
      throw new IllegalArgumentException("That is not a valid name.");
    }

    this.name = name;
  }

  /**
   * Verifies the name is valid based on regex
   *
   * @param name proposed name of player
   * @return returns whether name is valid or not.
   */
  public static boolean isNameValid(String name) {
    return name != null && name.matches("[a-zA-Z0-9 ]+");
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Player player = (Player) o;
    return equals(player);
  }

  public boolean equals(Player player) {
    return Objects.equals(name, player.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }
}
