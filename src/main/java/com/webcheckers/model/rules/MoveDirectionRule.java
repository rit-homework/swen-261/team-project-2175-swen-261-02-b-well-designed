package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.view.Piece.Color;

/** A rule to validate that the move is in the correct direction. */
public class MoveDirectionRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color color, Board board) {
    int rowDelta = move.getRowDiff();

    if (rowDelta < 0 && color == Color.WHITE) {
      // White Pieces Should Move Away From Origin
      return MoveInvalidReason.MOVED_BACKWARDS;
    }

    if (rowDelta > 0 && color == Color.RED) {
      // Red Pieces Should Move Towards Origin
      return MoveInvalidReason.MOVED_BACKWARDS;
    }

    return null;
  }
}
