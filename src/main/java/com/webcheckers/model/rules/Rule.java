package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.view.Piece.Color;

/** A rule for checking whether a move is valid or not. */
public abstract class Rule {

  /**
   * Checks whether the move violates this rule.
   *
   * @param move The move to check.
   * @param playerColor The color of the player who made the move.
   * @param board The board the move is made on. This shouldn't be mutated.
   * @return The reason the move is invalid or null if the move is valid.
   */
  public abstract MoveInvalidReason checkMove(Move move, Color playerColor, Board board);

  /**
   * Checks whether a move made on a board by playerColor violates any of the rules provided.
   *
   * @param move The move to check against all rules.
   * @param board The board the move is made in the context of.
   * @param playerColor The color of the player making the move.
   * @param rules The rules to check for violation against.
   * @return The reason why the move is invalid or null if the move is valid.
   */
  public static MoveInvalidReason violatesOneOf(
      Move move, Board board, Color playerColor, Rule[] rules) {
    for (Rule rule : rules) {
      MoveInvalidReason reason = rule.checkMove(move, playerColor, board);
      if (reason != null) {
        return reason;
      }
    }

    return null;
  }
}
