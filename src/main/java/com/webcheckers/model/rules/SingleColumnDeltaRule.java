package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.view.Piece.Color;

/** A rule to validate that a piece moves only a single column. */
public class SingleColumnDeltaRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color playerColor, Board board) {
    int colDelta = move.getColDelta();
    if (colDelta != 1) {
      return MoveInvalidReason.MOVED_TOO_MANY_COLS;
    }

    return null;
  }
}
