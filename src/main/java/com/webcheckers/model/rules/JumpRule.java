package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Piece;
import com.webcheckers.model.view.Piece.Color;

/**
 * Checks that a move is a valid jump move. This means it is two spaces and jumps a piece non-empty
 * piece of the opponent.
 */
public class JumpRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color playerColor, Board board) {
    int colDelta = move.getColDiff();
    int rowDelta = move.getRowDiff();

    if ((Math.abs(colDelta) != 2) || (Math.abs(rowDelta) != 2)) {
      return MoveInvalidReason.JUMP_NOT_TWO_SPACES;
    }

    int jumpedColDelta = colDelta / 2;
    int jumpedRowDelta = rowDelta / 2;

    Position start = move.getStart();

    int jumpedRow = jumpedRowDelta + start.getRow();
    int jumpedCol = jumpedColDelta + start.getCell();

    Piece toJumpPiece = board.getPiece(new Position(jumpedRow, jumpedCol));
    if (toJumpPiece == null) {
      return MoveInvalidReason.NO_PIECE_TO_JUMP;
    }

    if (toJumpPiece.getColor() == playerColor) {
      return MoveInvalidReason.JUMP_PIECE_SAME_COLOR;
    }

    return null;
  }
}
