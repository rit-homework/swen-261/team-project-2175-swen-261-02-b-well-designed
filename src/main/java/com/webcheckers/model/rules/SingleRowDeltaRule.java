package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.view.Piece.Color;

/** A rule to validate that the piece only moves a single row. */
public class SingleRowDeltaRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color playerColor, Board board) {
    int rowDelta = move.getRowDelta();

    if (rowDelta != 1) {
      return MoveInvalidReason.MOVED_TOO_MANY_ROWS;
    }

    return null;
  }
}
