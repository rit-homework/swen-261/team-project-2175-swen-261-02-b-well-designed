package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Piece;
import com.webcheckers.model.view.Piece.Color;

/**
 * A rule to validate the piece at the start position of the move is owned by the player making the
 * move.
 */
public class StartOwnedRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color playerColor, Board board) {
    Position startPosition = move.getStart();
    Piece startPiece = board.getPiece(startPosition);

    if (startPiece == null) {
      return MoveInvalidReason.NO_START_PIECE;
    }

    if (startPiece.getColor() != playerColor) {
      return MoveInvalidReason.NOT_PLAYERS_PIECE;
    }

    return null;
  }
}
