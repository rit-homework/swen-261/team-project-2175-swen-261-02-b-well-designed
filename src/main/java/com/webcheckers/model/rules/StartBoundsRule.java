package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Piece.Color;

/** Validates that the start position of the move is within the bounds of the board. */
public class StartBoundsRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color playerColor, Board board) {
    Position start = move.getStart();
    if (!start.withinBounds()) {
      return MoveInvalidReason.START_BOUNDS;
    }

    return null;
  }
}
