package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Piece;
import com.webcheckers.model.view.Piece.Color;

/** A rule to validate the end position is empty. */
public class EndEmptyRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color playerColor, Board board) {
    Position end = move.getEnd();
    Piece endPiece = board.getPiece(end);

    if (endPiece != null) {
      return MoveInvalidReason.END_NOT_EMPTY;
    }

    return null;
  }
}
