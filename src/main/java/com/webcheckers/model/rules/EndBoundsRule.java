package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.Position;
import com.webcheckers.model.view.Piece.Color;

/** Validates that the end position of the move is within the bounds of the board. */
public class EndBoundsRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color playerColor, Board board) {
    Position end = move.getEnd();
    if (!end.withinBounds()) {
      return MoveInvalidReason.END_BOUNDS;
    }

    return null;
  }
}
