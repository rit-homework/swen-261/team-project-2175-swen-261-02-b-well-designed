package com.webcheckers.model.rules;

import com.webcheckers.model.Board;
import com.webcheckers.model.Move;
import com.webcheckers.model.MoveInvalidReason;
import com.webcheckers.model.view.Piece.Color;

/** A rule validating that the move ends on a black tile. */
public class WhiteTileRule extends Rule {

  @Override
  public MoveInvalidReason checkMove(Move move, Color playerColor, Board board) {
    boolean isBlackSquare = move.getEnd().isBlackSquare();
    if (!isBlackSquare) {
      return MoveInvalidReason.END_WHITE_TILE;
    }

    return null;
  }
}
