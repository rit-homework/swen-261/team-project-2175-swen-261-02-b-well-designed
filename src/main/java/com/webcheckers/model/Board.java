package com.webcheckers.model;

import com.webcheckers.model.rules.Rule;
import com.webcheckers.model.view.Piece;
import com.webcheckers.model.view.Piece.Color;
import com.webcheckers.model.view.Space;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Holds the state of the game. */
public class Board {

  /** The size of a board. */
  private static final int BOARD_SIZE = 8;

  /** The number of rows in a board. */
  public static final int BOARD_ROWS = BOARD_SIZE;

  /** The number of columns in a board. */
  public static final int BOARD_COLS = BOARD_SIZE;

  private static int[][] directions = new int[][] {{1, 1}, {1, -1}, {-1, 1}, {-1, -1}};

  /**
   * The game board. The origin is at the top left corner. The board is viewed as from the side of
   * the first player.
   */
  final Piece[][] pieces = new Piece[BOARD_ROWS][BOARD_COLS];

  /**
   * @return A copy of the pieces in the board. Changes in this copy don't affect the board state.
   */
  public Piece[][] getPieces() {
    return Arrays.stream(pieces).map(Piece[]::clone).toArray(Piece[][]::new);
  }

  /** Creates a new empty board. */
  Board() {}

  /** Creates a board and initializes with {@link #initializeBoard()}. */
  static Board createBoard() {
    Board board = new Board();
    board.initializeBoard();
    return board;
  }

  /**
   * Creates a board and populates it using {@link BoardStringFactory#populateFromString(Board,
   * String)}.
   *
   * @param source The source from which the board is created.
   * @return The newly created board.
   */
  public static Board fromString(String source) {
    return BoardStringFactory.fromString(source);
  }

  /**
   * Initializes board to the starting state. There will be 12 pieces in the first 12 spaces of each
   * side. Here's a diagram of what the board will look like:
   *
   * <pre>
   *    01234567
   *   .--------.
   * 0 | w w w w|
   * 1 |w w w w |
   * 2 | w w w w|
   * 3 |        |
   * 4 |        |
   * 5 |r r r r |
   * 6 | r r r r|
   * 7 |r r r r |
   *   .--------.
   * </pre>
   */
  private void initializeBoard() {
    // Populate the first three rows.
    for (int rowIdx = 0; rowIdx < 3; rowIdx++) {
      populateRow(rowIdx, Color.WHITE);
    }

    // Populate the last three rows.
    for (int rowIdx = BOARD_ROWS - 3; rowIdx < BOARD_ROWS; rowIdx++) {
      populateRow(rowIdx, Color.RED);
    }
  }

  /**
   * Populates all valid squares in the row with pieces of the desired color.
   *
   * @param rowIdx The index of the row to populate.
   * @param color The color of the pieces to fill the row with.
   */
  private void populateRow(int rowIdx, Piece.Color color) {
    Piece[] row = pieces[rowIdx];

    for (int colIdx = 0; colIdx < row.length; colIdx++) {
      if (!Space.isBlackSquare(rowIdx, colIdx)) {
        // Can't place anything here, skip.
        continue;
      }

      row[colIdx] = new Piece(color);
    }
  }

  /**
   * Safely gets a piece from the board. If the position is invalid, null is returned. If the piece
   * is unset, returns null.
   *
   * @param position The position on the board to get the piece from.
   * @return The piece at the position or null if the position is invalid or the space is empty.
   */
  public Piece getPiece(Position position) {
    if (!position.withinBounds()) {
      return null;
    }

    int row = position.getRow();
    int col = position.getCell();

    return pieces[row][col];
  }

  /**
   * Sets the piece at the position of the board. If the position is out of bounds, throws an
   * exception.
   *
   * @param position The position on the board to set the piece.
   * @param piece The piece to set a the position.
   */
  void setPiece(Position position, Piece piece) {
    if (!position.withinBounds()) {
      throw new IllegalArgumentException("The position is out of bounds.");
    }

    int row = position.getRow();
    int col = position.getCell();

    pieces[row][col] = piece;
  }

  /**
   * Get all the pieces which are a certain color along with their positions.
   *
   * @param color The color of the pieces to get all of.
   * @return A stream of pieces along with their position information.
   */
  Stream<DoubleIndexedContainer<Piece>> getPiecesByColor(Color color) {
    return DoubleIndexedContainer.streamFrom2D(pieces)
        .filter(
            container -> {
              Piece piece = container.value;

              return piece != null && piece.getColor() == color;
            });
  }

  @Override
  public String toString() {
    return Arrays.stream(pieces)
        .map(row -> Arrays.stream(row).map(Piece::asString).collect(Collectors.joining()))
        .collect(Collectors.joining("\n"));
  }

  /**
   * Applies a move to the board, changing the state. Moves the piece from the start position to the
   * end position, removing the jumped piece if the move is a jump move. Kings pieces which are
   * moved to the king row. This method assumes the move is valid.
   *
   * @param move The move to apply to the board.
   */
  public void applyMove(Move move) {
    Piece pieceToMove = getPiece(move.getStart());

    Position start = move.getStart();
    Position end = move.getEnd();

    if (move.getMoveType() == MoveType.JUMP) {
      Position jumpedPosition = move.getJumpedPosition();
      setPiece(jumpedPosition, null);
    }

    setPiece(end, pieceToMove);
    setPiece(start, null);
  }

  /**
   * Kings any pieces in the king row for the player of color. This allows for disallowing jump
   * moves to a king position.
   *
   * @param color The color of pieces to king if possible.
   */
  void kingPieces(Color color) {
    int lastRow = color.getLastRow();

    getPiecesByColor(color)
        .filter(container -> container.row == lastRow)
        .forEach(container -> container.value.makeKing());
  }

  /**
   * Revert the board to the state before the move was made.
   *
   * @param move The move to revert.
   */
  public void undoMove(Move move) {
    Piece movedPiece = getPiece(move.getEnd());
    Color movedPieceColor = movedPiece.getColor();

    Position start = move.getStart();
    Position end = move.getEnd();

    if (move.getMoveType() == MoveType.JUMP) {
      Position jumpedPosition = move.getJumpedPosition();
      setPiece(jumpedPosition, new Piece(movedPieceColor.getOther()));
    }

    // Move Piece From End Position To Start Position
    setPiece(start, movedPiece);
    setPiece(end, null);

    // Un King Piece
    int endRow = end.getRow();
    if (endRow == movedPieceColor.getLastRow()) {
      movedPiece.unKing();
    }
  }

  /**
   * Creates a move starting from the start position and ending at the start position offset by the
   * direction argument.
   *
   * @param startPosition The position the move will start from.
   * @param direction The offset the move will be made in.
   * @return A move starting from the start position and ending at the start position plus an
   *     offset.
   */
  private static Move getMoveInDirection(Position startPosition, int[] direction) {
    int rowDelta = direction[0];
    int colDelta = direction[1];

    Position endPosition = startPosition.add(rowDelta, colDelta);
    return new Move(startPosition, endPosition);
  }

  /**
   * Ensures that a piece exists at a position. Otherwise, throws an IllegalArgumentException.
   *
   * @param position The position to check for a piece at.
   */
  void ensurePieceAtPosition(Position position) {
    if (getPiece(position) == null) {
      throw new IllegalArgumentException("There isn't a piece at the start position.");
    }
  }

  /**
   * @param position the position of the piece to check.
   * @return Returns true if a piece can move.
   */
  private boolean pieceCanMove(Position position) {
    return pieceCanSimpleMove(position) || pieceCanJump(position);
  }

  /**
   * Validates whether the piece at the position can make a regular move.
   *
   * @param position The position of the piece on the board.
   * @return true if a simple move is possible.
   */
  private boolean pieceCanSimpleMove(Position position) {
    ensurePieceAtPosition(position);
    Piece piece = getPiece(position);
    Color pieceColor = piece.getColor();

    return Arrays.stream(directions)
        .map(direction -> getMoveInDirection(position, direction))
        .anyMatch(
            move -> {
              Rule[] rules = MoveValidator.getRules(MoveType.SIMPLE, move, this);
              return Rule.violatesOneOf(move, this, pieceColor, rules) == null;
            });
  }

  /**
   * Validates whether the piece at the position can make a jump move.
   *
   * @param position The position of the piece on the board.
   * @return true if jump is possible.
   */
  public boolean pieceCanJump(Position position) {
    ensurePieceAtPosition(position);
    Piece piece = getPiece(position);
    Color pieceColor = piece.getColor();

    return Arrays.stream(directions)
        .map(directions -> Arrays.stream(directions).map(i -> i * 2).toArray())
        .map(direction -> getMoveInDirection(position, direction))
        .anyMatch(
            move -> {
              Rule[] rules = MoveValidator.getRules(MoveType.JUMP, move, this);
              return Rule.violatesOneOf(move, this, pieceColor, rules) == null;
            });
  }

  /**
   * Returns true if the player can make a move.
   *
   * @param color The color of the player to check.
   * @return True if the player can make a move.
   */
  boolean playerCanMove(Color color) {
    return getPiecesByColor(color)
        .anyMatch(
            container -> {
              Position position = new Position(container.row, container.col);
              return pieceCanMove(position);
            });
  }
}
