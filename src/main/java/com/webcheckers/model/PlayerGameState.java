package com.webcheckers.model;

import java.util.ArrayDeque;
import java.util.Deque;

/** A container for a player's state associated with a game. */
public class PlayerGameState {

  /** The player this instance holds state for. */
  private final Player player;

  /** The moves which were last validated for this player. */
  private Deque<Move> lastValidatedMoveStack = new ArrayDeque<>();

  PlayerGameState(Player player) {
    this.player = player;
  }

  public Player getPlayer() {
    return player;
  }

  /**
   * Pop last validated move off the stack.
   *
   * @return Move on top of the last validated stack before popping.
   */
  public Move popLastValidatedMove() {
    return lastValidatedMoveStack.pop();
  }

  /**
   * Check whether there are move(s) on the stack.
   *
   * @return whether or not the stack of past validated moves is empty.
   */
  public boolean isLastValidatedMoveStackEmpty() {
    return lastValidatedMoveStack.isEmpty();
  }

  /** Empty the stack of validated moves. */
  public void emptyMoveStack() {
    lastValidatedMoveStack = new ArrayDeque<>();
  }

  /**
   * Add another move to the move stack
   *
   * @param lastValidatedMove move to be put on the stack
   */
  public void pushLastValidatedMove(Move lastValidatedMove) {
    lastValidatedMoveStack.push(lastValidatedMove);
  }

  /**
   * Peek top of validated moves stack.
   *
   * @return Previously validated move
   */
  public Move peekLastValidatedMove() {
    return lastValidatedMoveStack.peek();
  }
}
