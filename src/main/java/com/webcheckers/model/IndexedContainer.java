package com.webcheckers.model;

import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * A helper class which holds a value along with its index.
 *
 * @param <T> The type of the value.
 */
public class IndexedContainer<T> {

  public final int index;
  public final T value;

  private IndexedContainer(int index, T value) {
    this.index = index;
    this.value = value;
  }

  /**
   * A factory to create a stream of indexed container's from an array.
   *
   * @param array The array to create the stream of indexed containers from.
   * @param <T> The type of elements in the array.
   * @return A stream of indexed container's with the index as the index of the item in the array
   *     and the item as the item at the index in the array.
   */
  public static <T> Stream<IndexedContainer<T>> streamFromArray(T[] array) {
    return IntStream.range(0, array.length)
        .mapToObj(idx -> new IndexedContainer<>(idx, array[idx]));
  }
}
