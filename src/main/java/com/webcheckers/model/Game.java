package com.webcheckers.model;

import com.webcheckers.model.view.Piece.Color;

/** A container for a game's state. */
public class Game {

  /** The red or first player. */
  private final PlayerGameState firstPlayer;

  /** The white or second player. */
  private final PlayerGameState secondPlayer;

  /** The current state of the board. */
  private final Board board;

  /** The player who's turn it currently is. */
  private Player activePlayer;

  /**
   * Whether or not the game is currently active. The game is active when two players are in the
   * game and playing.
   */
  private boolean isActive = true;

  /** The player which has lost the game. Null while the game is active, otherwise the loser. */
  private Player loser;

  /**
   * Creates a new game.
   *
   * @param firstPlayer The first player associated with the game.
   * @param secondPlayer The second player associated with the game.
   * @param board The board this game is played on.
   */
  private Game(Player firstPlayer, Player secondPlayer, Board board) {
    this.firstPlayer = new PlayerGameState(firstPlayer);
    this.secondPlayer = new PlayerGameState(secondPlayer);
    this.board = board;
  }

  /**
   * Creates a game with the default board.
   *
   * @see #createGameWithBoard(Player, Player, Board)
   * @return A newly created game.
   */
  public static Game createGame(Player firstPlayer, Player secondPlayer) {
    Board board = Board.createBoard();
    return createGameWithBoard(firstPlayer, secondPlayer, board);
  }

  /**
   * A factory to create a game and set the active player.
   *
   * @param firstPlayer The first player.
   * @param secondPlayer The second player.
   * @param board The board the game is played on.
   * @return A newly created game.
   */
  public static Game createGameWithBoard(Player firstPlayer, Player secondPlayer, Board board) {
    Game game = new Game(firstPlayer, secondPlayer, board);
    game.activePlayer = firstPlayer;

    return game;
  }

  /** @return Gets the first player (red) player. */
  public Player getFirstPlayer() {
    return firstPlayer.getPlayer();
  }

  /** @return Gets the second player (white) player. */
  public Player getSecondPlayer() {
    return secondPlayer.getPlayer();
  }

  /**
   * A helper method to get the state associated with a player.
   *
   * @param player The player to retrive the state of.
   * @return The state of the player.
   */
  public PlayerGameState getStateForPlayer(Player player) {
    ensurePlayerInGame(player);

    if (player == firstPlayer.getPlayer()) {
      return firstPlayer;
    }

    return secondPlayer;
  }

  /** @return Gets the board. */
  public Board getBoard() {
    return board;
  }

  /**
   * Gets the color of a player.
   *
   * @param player A player participating in the game.
   * @return The color of the player.
   */
  public Color getColor(Player player) {
    ensurePlayerInGame(player);

    if (player == firstPlayer.getPlayer()) {
      return Color.RED;
    }

    return Color.WHITE;
  }

  /**
   * Checks whether a move is valid.
   *
   * @param move The move to check. This move must not have null required fields.
   * @param player The player making the move.
   * @return The reason why the move is invalid or null if the move is valid.
   */
  public MoveInvalidReason validateMove(Move move, Player player) {
    Color playerColor = getColor(player);
    return MoveValidator.validateMove(move, getBoard(), playerColor);
  }

  /** @return The player who's turn it currently is. */
  public Player getActivePlayer() {
    return activePlayer;
  }

  /**
   * Whether or not it is the turn of the specified player.
   *
   * @param player The player to check.
   * @return True if it is the player's turn, false otherwise.
   */
  public boolean isActivePlayer(Player player) {
    return activePlayer.equals(player);
  }

  /** Changes the currently active player to the other player. */
  public void nextTurn() {
    if (activePlayer == firstPlayer.getPlayer()) {
      activePlayer = secondPlayer.getPlayer();
    } else {
      activePlayer = firstPlayer.getPlayer();
    }
  }

  /**
   * Checks whether a player is in the game.
   *
   * @param player The player to check participation of.
   * @return Whether or not the player is in the game.
   */
  public boolean playerInGame(Player player) {
    return getFirstPlayer().equals(player) || getSecondPlayer().equals(player);
  }

  /**
   * Throws an error if the player isn't in the game.
   *
   * @param player The player to check participation of.
   */
  void ensurePlayerInGame(Player player) {
    if (!playerInGame(player)) {
      throw new IllegalArgumentException("That player isn't part of the game.");
    }
  }

  /**
   * Ends the game, marking a player as a loser.
   *
   * @param loser The player which lost the game.
   */
  public void endGameWithLoser(Player loser) {
    ensurePlayerInGame(loser);
    endGameWithTie();

    this.loser = loser;
  }

  /**
   * Ends the game, marking a player as the winner.
   *
   * @param winner The player which won the game.
   */
  public void endGameWithWinner(Player winner) {
    ensurePlayerInGame(winner);
    endGameWithTie();

    this.loser = getOtherPlayer(winner);
  }

  /**
   * Given a player this game, get's the other player in this game.
   *
   * @param player A player in this game.
   * @return The other player in this game.
   */
  public Player getOtherPlayer(Player player) {
    ensurePlayerInGame(player);

    if (player.equals(secondPlayer.getPlayer())) {
      return firstPlayer.getPlayer();
    }

    return secondPlayer.getPlayer();
  }

  /** Ends the game as a tie. */
  private void endGameWithTie() {
    isActive = false;
  }

  /**
   * @return Whether or not the game is active.
   * @see #isActive
   */
  public boolean isActive() {
    return isActive;
  }

  /** @return The player that isn't the loser. */
  Player getWinner() {
    return getOtherPlayer(loser);
  }

  /** @return The player which has lost the game. */
  public Player getLoser() {
    return loser;
  }

  /**
   * Kings any pieces in the king row for the player of color. This allows for disallowing jump
   * moves from a king position.
   *
   * @param player The player to king if possible.
   */
  public void kingPieces(Player player) {
    board.kingPieces(getColor(player));
  }

  /**
   * Returns true if the player can make a move.
   *
   * @param player The player to check.
   * @return True if the player can make a move.
   */
  public boolean playerCanMove(Player player) {
    return board.playerCanMove(getColor(player));
  }
}
