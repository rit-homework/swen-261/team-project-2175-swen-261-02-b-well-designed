package com.webcheckers.model;

import com.webcheckers.model.view.Piece.Color;

public class Move {

  /** The starting position of the move. */
  private final Position start;

  /** The ending position of the move. */
  private final Position end;

  public Move(Position start, Position end) {
    this.start = start;
    this.end = end;
  }

  /**
   * Gets the type of move. A move is a jump move if it moves both three columns and three rows,
   * otherwise, it is a simple move.
   *
   * @return The type of move.
   */
  public MoveType getMoveType() {
    if (getColDelta() == 2 && getRowDelta() == 2) {
      return MoveType.JUMP;
    }

    return MoveType.SIMPLE;
  }

  /** @return The start position of the move. */
  public Position getStart() {
    return start;
  }

  /** @return The end position of the move. */
  public Position getEnd() {
    return end;
  }

  /**
   * @return If this is a jump move, the position of the piece being jumped over. Otherwise, null.
   */
  Position getJumpedPosition() {
    if (getMoveType() != MoveType.JUMP) {
      return null;
    }

    int row = (start.getRow() + end.getRow()) / 2;
    int col = (start.getCell() + end.getCell()) / 2;
    return new Position(row, col);
  }

  /** @return The difference between the end position's row and the start position's row. */
  public int getRowDiff() {
    return end.getRow() - start.getRow();
  }

  /** @return The distance between the start and end row. */
  public int getRowDelta() {
    return Math.abs(getRowDiff());
  }

  /** @return The distance between the start and end column. */
  public int getColDelta() {
    return Math.abs(getColDiff());
  }

  /** @return The difference between the end position's column and the start position's column. */
  public int getColDiff() {
    return end.getCell() - start.getCell();
  }

  /**
   * @return Whether or not the move is valid. This is needed because a possibly invalid move could
   *     be created when GSON deserializes.
   */
  public boolean containsRequiredFields() {
    return start != null && end != null;
  }

  /**
   * Flips the start position and end position of the move.
   *
   * @return A flipped move.
   */
  private Move flipMove() {
    Position start = getStart();
    Position flippedStart = start.flip();

    Position end = getEnd();
    Position flippedEnd = end.flip();

    return new Move(flippedStart, flippedEnd);
  }

  /**
   * Converts the move to the coordinate system of the red player.
   *
   * @param playerColor The color of the player from which the move is made.
   * @return The move from the coordinate system of the red player.
   */
  public Move fromPlayerPerspective(Color playerColor) {
    if (playerColor == Color.RED) {
      return this;
    }

    return flipMove();
  }
}
