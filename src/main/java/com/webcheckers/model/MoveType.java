package com.webcheckers.model;

import com.webcheckers.model.rules.EndBoundsRule;
import com.webcheckers.model.rules.EndEmptyRule;
import com.webcheckers.model.rules.JumpRule;
import com.webcheckers.model.rules.MoveDirectionRule;
import com.webcheckers.model.rules.Rule;
import com.webcheckers.model.rules.SingleColumnDeltaRule;
import com.webcheckers.model.rules.SingleRowDeltaRule;
import com.webcheckers.model.rules.StartBoundsRule;
import com.webcheckers.model.rules.StartOwnedRule;
import com.webcheckers.model.rules.WhiteTileRule;

public enum MoveType {
  /** A list of rules which jump moves are validated against. */
  JUMP(
      new Rule[] {
        new StartBoundsRule(),
        new EndBoundsRule(),
        new WhiteTileRule(),
        new StartOwnedRule(),
        new JumpRule(),
        new MoveDirectionRule(),
        new EndEmptyRule()
      },
      new Rule[] {
        new StartBoundsRule(),
        new EndBoundsRule(),
        new WhiteTileRule(),
        new StartOwnedRule(),
        new JumpRule(),
        new EndEmptyRule()
      }),

  /** The list of rules simple moves are validated against. */
  SIMPLE(
      new Rule[] {
        new StartBoundsRule(),
        new EndBoundsRule(),
        new WhiteTileRule(),
        new StartOwnedRule(),
        new MoveDirectionRule(),
        new SingleRowDeltaRule(),
        new SingleColumnDeltaRule(),
        new EndEmptyRule()
      },
      new Rule[] {
        new StartBoundsRule(),
        new EndBoundsRule(),
        new WhiteTileRule(),
        new StartOwnedRule(),
        new SingleRowDeltaRule(),
        new SingleColumnDeltaRule(),
        new EndEmptyRule()
      });

  private final Rule[] rules;
  private final Rule[] kingRules;

  MoveType(Rule[] rules, Rule[] kingRules) {
    this.rules = rules;
    this.kingRules = kingRules;
  }

  public Rule[] getRules() {
    return rules;
  }

  public Rule[] getKingRules() {
    return kingRules;
  }
}
