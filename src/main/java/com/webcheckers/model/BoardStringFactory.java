package com.webcheckers.model;

import com.webcheckers.model.view.Piece;
import com.webcheckers.model.view.Piece.Color;

/** A helper class to create a board from a string. */
public class BoardStringFactory {

  static Board fromString(String source) {
    Board board = new Board();
    populateFromString(board, source);

    return board;
  }

  /**
   * Populates the board with information from the string. The string must be an 8x8, newline
   * separated grid of characters. If a position contains the 'r' character, a red piece is placed.
   * If a position contains the 'w' character, a white piece is placed there. If neither of these
   * characters is at a position, that position is set to null.
   *
   * @param board The board to populate.
   * @param source The source string from which the Board is populated from.
   */
  private static void populateFromString(Board board, String source) {
    String[] lines = source.split("\n");
    if (lines.length != Board.BOARD_ROWS) {
      throw new IllegalArgumentException(
          String.format("The string must have exactly %d lines.", Board.BOARD_ROWS));
    }

    for (int rowIdx = 0; rowIdx < lines.length; rowIdx++) {
      String row = lines[rowIdx];
      if (row.length() != Board.BOARD_COLS) {
        throw new IllegalArgumentException(
            String.format("The string must have exactly %d columns.", Board.BOARD_COLS));
      }

      for (int colIdx = 0; colIdx < row.length(); colIdx++) {
        char khar = Character.toLowerCase(row.charAt(colIdx));

        Piece piece = null;
        if (khar == 'r') {
          // Put A Red Piece Here
          piece = new Piece(Color.RED);
        } else if (khar == 'w') {
          piece = new Piece(Color.WHITE);
        }

        board.pieces[rowIdx][colIdx] = piece;
      }
    }
  }
}
