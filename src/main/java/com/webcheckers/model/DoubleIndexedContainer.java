package com.webcheckers.model;

import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * A helper class for consuming 2d arrays with streams.
 *
 * @param <T> The type of the value to hold.
 */
class DoubleIndexedContainer<T> {

  final int row;
  final int col;
  final T value;

  private DoubleIndexedContainer(int row, int col, T value) {
    this.row = row;
    this.col = col;
    this.value = value;
  }

  /**
   * A factory to create a {@link DoubleIndexedContainer} of every character in a list of strings.
   * The the row is the index of the string in the list of strings. The column, is the index of the
   * character in each string.
   *
   * @param array The array of strings to create containers from.
   * @return A stream of all characters and their indices.
   */
  static Stream<DoubleIndexedContainer<Character>> streamFromStringArray(String[] array) {
    return IndexedContainer.streamFromArray(array).flatMap(DoubleIndexedContainer::stitchString);
  }

  /**
   * Given an indexed container of a string, generates {@link DoubleIndexedContainer}s for each
   * character in the string with the row as the index of the {@link IndexedContainer} and the
   * column as the index of the character in the string.
   *
   * @param container The container to generate DoubleIndexedContainers for.
   * @return {@link DoubleIndexedContainer}s for each character in the string.
   */
  private static Stream<DoubleIndexedContainer<Character>> stitchString(
      IndexedContainer<String> container) {
    return IntStream.range(0, container.value.length())
        .mapToObj(
            colIdx ->
                new DoubleIndexedContainer<>(
                    container.index,
                    colIdx,
                    Character.toLowerCase(container.value.charAt(colIdx))));
  }

  /**
   * Given a 2D array, creates a stream of DoubleIndexedContainer's with for each position with the
   * value at the position.
   *
   * @param array The 2d array.
   * @param <T> The type of element in to 2d array.
   * @return A stream of every element in the array along with its position.
   */
  static <T> Stream<DoubleIndexedContainer<T>> streamFrom2D(T[][] array) {
    return IndexedContainer.streamFromArray(array).flatMap(DoubleIndexedContainer::row);
  }

  /**
   * Given a row of a 2d array indexed by rowContainer, returns a stream of double indexed containrs
   * for that row.
   *
   * @param rowContainer A container with the row index and row's elements to create
   *     DoubleIndexedContainers from.
   * @param <T> The type of elements in the indexed row.
   * @return A stream of double indexed containrs for the specified row.
   */
  private static <T> Stream<DoubleIndexedContainer<T>> row(IndexedContainer<T[]> rowContainer) {
    return IndexedContainer.streamFromArray(rowContainer.value)
        .map(colContainer -> col(rowContainer, colContainer));
  }

  /**
   * Creates the DoubleIndexedContainer for a single position in a two dimensional array.
   *
   * @param rowContainer A container with the row values and index.
   * @param colContainer A container with the column value and index.
   * @param <T> The type of element.
   * @return A DoubleIndexedContainer for a single position in the 2d array.
   */
  private static <T> DoubleIndexedContainer<T> col(
      IndexedContainer<T[]> rowContainer, IndexedContainer<T> colContainer) {
    return new DoubleIndexedContainer<>(rowContainer.index, colContainer.index, colContainer.value);
  }
}
