package com.webcheckers.model;

import com.webcheckers.model.view.Space;
import java.util.Objects;

public class Position {

  /** The row index of this position. A number from 0-7. */
  private final int row;

  /** The column index of this position. A number from 0-7. */
  private final int cell;

  public Position(int row, int cell) {
    this.row = row;
    this.cell = cell;
  }

  public int getCell() {
    return cell;
  }

  public int getRow() {
    return row;
  }

  /**
   * Checks whether the tile at the position is black or white.
   *
   * @return True if the space corresponding with this position is black.
   * @see Space#isBlackSquare
   */
  public boolean isBlackSquare() {
    return Space.isBlackSquare(row, cell);
  }

  /**
   * Checks whether this position is within the bounds of an 8x8 checker board.
   *
   * @return true, if within bounds otherwise, false.
   */
  public boolean withinBounds() {
    return row >= 0 && row < Board.BOARD_ROWS && cell >= 0 && cell < Board.BOARD_COLS;
  }

  /** @return Rotates the position 180 degrees. */
  Position flip() {
    return new Position(Board.BOARD_ROWS - 1 - row, Board.BOARD_COLS - 1 - cell);
  }

  /**
   * Creates a new position translated by the arguments.
   *
   * @param rowDelta The number of rows the new position is from the old position.
   * @param colDelta the number of columns the new position is from the old position.
   * @return A new position.
   */
  public Position add(int rowDelta, int colDelta) {
    return new Position(row + rowDelta, cell + colDelta);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Position)) {
      return false;
    }

    Position position = (Position) o;
    return equals(position);
  }

  public boolean equals(Position other) {
    return row == other.row && cell == other.cell;
  }

  @Override
  public int hashCode() {
    return Objects.hash(row, cell);
  }
}
