package com.webcheckers.model;

import com.webcheckers.model.rules.Rule;
import com.webcheckers.model.view.Piece;
import com.webcheckers.model.view.Piece.Color;
import com.webcheckers.model.view.Piece.Type;

public class MoveValidator {

  /**
   * Checks whether a move is valid.
   *
   * @param move The move to check.
   * @param board The board to check the move against.
   * @param playerColor The color of the player making the move.
   * @return The reason why the move is invalid or null if the move is valid.
   */
  public static MoveInvalidReason validateMove(Move move, Board board, Color playerColor) {
    if (!move.containsRequiredFields()) {
      throw new IllegalArgumentException("The move violates what a valid move is.");
    }

    MoveType type = getNextMoveType(playerColor, board);
    Rule[] rules = getRules(type, move, board);
    return Rule.violatesOneOf(move, board, playerColor, rules);
  }

  /**
   * Get's the type of the next move.
   *
   * @param playerColor The color of the player making the move.
   * @param board The board the game is being played on.
   * @return The type of move which must be made for the next move.
   */
  private static MoveType getNextMoveType(Color playerColor, Board board) {
    if (MoveValidator.isJumpPossible(playerColor, board)) {
      return MoveType.JUMP;
    }

    return MoveType.SIMPLE;
  }

  /**
   * Gets the array of rules which should be applied to a move.
   *
   * @param type The type of move to make.
   * @param move The move to check.
   * @param board The board to check the move against.
   * @return The rules which will be applied to the move.
   */
  static Rule[] getRules(MoveType type, Move move, Board board) {
    Position start = move.getStart();
    Piece piece = board.getPiece(start);
    if (piece.getType() == Type.KING) {
      return type.getKingRules();
    }

    return type.getRules();
  }

  /**
   * Check's whether the player corresponding to the color can make a single jump move.
   *
   * @param color The color of the player making a move.
   * @param board The board on which the move is being made.
   * @return Whether or not a jump move is possible.
   */
  static boolean isJumpPossible(Color color, Board board) {
    return board
        .getPiecesByColor(color)
        .anyMatch(container -> board.pieceCanJump(new Position(container.row, container.col)));
  }
}
