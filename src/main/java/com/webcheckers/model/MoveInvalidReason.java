package com.webcheckers.model;

public enum MoveInvalidReason {
  START_BOUNDS("the start position of the move is outside the board"),
  END_BOUNDS("the end position of the move is outside the board"),

  NO_START_PIECE("the start position of the move is empty"),
  NOT_PLAYERS_PIECE("the piece at the start position isn't yours"),
  END_WHITE_TILE("the piece moves to a white tile"),
  MOVED_BACKWARDS("the piece moved in the wrong direction"),
  MOVED_TOO_MANY_ROWS("the piece moved too many rows"),
  MOVED_TOO_MANY_COLS("the piece moved too many columns"),
  JUMP_NOT_TWO_SPACES("piece didn't jump diagonally two spaces"),
  NO_PIECE_TO_JUMP("there is no piece to jump over"),
  JUMP_PIECE_SAME_COLOR("jumping piece with of same color"),
  END_NOT_EMPTY("the end position already has a piece");

  /**
   * The reason the move is invalid. The following substitution should read as a sentence: That move
   * is invalid because ${reason}.
   */
  private final String reason;

  MoveInvalidReason(String reason) {
    this.reason = reason;
  }

  public String getReasonSentence() {
    return String.format("That move is invalid because %s.", reason);
  }
}
