# WebCheckers Home

Welcome to the WebCheckers Project!

## Team

* Martin Charles
* Brad Campbell
* Brian Eckam
* Sam Lipton
* Justin Schneider

## [Design Documentation](./DesignDoc)

Click above for details of the design documentation.

## [Setup Guide](./SetupGuide)

Click above for details about how to setup your development environment to work on this project.
