---
geometry: margin=1in
---

# Web Checkers Design Documentation

# Team Information
* Team name: Well Designed

* Team Members
  * Martin Charles
  * Brad Campbell
  * Brian Eckam
  * Sam Lipton
  * Justin Schneider

## Executive Summary

Web Checkers is a web-based, multi-player checkers application with asynchronous
play and support for multiple games.

The application allows players to play checkers with other players who are
currently signed into the server. The user interface uses a browser with drag
and drop capabilities for making moves.

### Purpose
We aim to give checkers players a place to play checkers with others.

### Glossary and Acronyms

| Term | Definition |
|------|------------|
| VO | Value Object |

## Requirements

This section describes the features of the application.

1. Every player must sign-in before playing a game, and be able to sign-out when
   finished playing.

2. Two players must be able to play a game of checkers based upon the [American
   rules][rules].

3. Either player of a game may choose to resign at any point in the game.

4. Asynchronous play. Player can start a game, sign out, sign back in and
   continue playing games.

5. Multiple games. A player me be a part of more than one game at a time.

### Definition of MVP

The MVP is made up of the first three requirements. Players should be able to
sign in, play a game following the [American rules][rules] and resign at any
point.

### MVP Features

The MVP features are the first three requirements.

### Roadmap of Enhancements

The last two requirements are enhancements. If there is additional time, we'd
like to add the following enhancements:

1. Replay Mode: Games can be stored and the replayed later.

2. Spectator Mode: Other players may view and on-going game that they are not
   playing.

## Application Domain

![WebCheckers Domain Model](domain-model.jpg)

Users sign in with their credentials (username). Once the user is signed in,
they are associated with a player entity. Players may participate in a game of
checkers with another player.

The Checkers Game is played on a board made up of 64 squares laid out in an 8x8
square. Tiles are colored in a checkerboard pattern with a black square at the
bottom left side of the board. Each square has a location which identifies where
on the board it is.

There are two players in a game. Each player owns 12 pieces to start placed on
the black squares closest to the player's king row. The first player is colored
red and the second, white. The pieces which have the same color as the player
are owned by the corresponding player.

There are two types of pieces: King and Pawn. A pawn becomes a king when it
reaches the row furthest from the one it started on. A pawn can only move
diagonally forwards on black squares one row per turn. A king can move both
diagonally forwards or backwards on black squares one row per turn. Other valid
moves include jumping pieces and chaining jump moves.

[Here are the American Checkers Rules this project implements.][rules]

## Architecture

This section describes the application architecture.

### Summary

The following Tiers/Layers model shows a high-level view of the webapp's
architecture.

![The Tiers & Layers of the Architecture](architecture-tiers-and-layers.png)

As a web application, the user interacts with the system using a browser. The
client-side of the UI is composed of HTML pages with some minimal CSS for
styling the page. There is also some JavaScript that has been provided to the
team by the architect.

The server-side tiers include the UI Tier that is composed of UI Controllers and
Views. Controllers are built using the Spark framework and View are built using
the FreeMarker framework. The Application and Model tiers are built using
plain-old Java objects (POJOs).

Details of the components within these tiers are supplied below.

### Overview of User Interface

This section describes the web interface flow; this is how the user views and
interacts with the WebCheckers application.

![The WebCheckers Web Interface Statechart](state-chart.png)

Once a user starts a connection to the server, they are redirected to the public
home page. From there the user can visit the sign in page. On the sign in page,
If the user enters an invalid name or a name already in use, then the user is
returned to the sign in page.

After the user signs in successfully, they are redirected to the game lobby
where they can see a list of other active players on the server. Choosing a
player will redirect both players to a game page. If the selected user is
already in a game then the user who chose the busy player will be redirected to
the lobby. On the game page, the players can play a game of checkers.

During the game, the players can choose to resign, at which point they will be
taken back to the game lobby. Players can also complete the game by winning or
loosing which will take then back to the game lobby.

### UI Tier

The UI tier contains code to display the current state of the application. This
tier is made up of routes which handle rendering the HTML to display in response
to a request at a url with a method and routes which respond to JSON AJAX
request. Routes are registered in the `WebServer` class.

Routes which render HTML implement the `TemplateViewRoute` interface. This
interface has a method which takes in a `Request` object, a `Response` object
for creating responses and returns a `ModelAndView`. The `ModelAndView` is
rendered using the FreeMarker template engine. Here's what registering a
`TemplateViewRoute` looks like:

    service.get("/", new GetHomeRoute(lobby), templateEngine);

Routes which respond to JSON requests extend the `JSONRoute` class. This class
handles parsing a JSON request, ensuring it is non-empty and passing the parsed
object to a handler. This class isn't responsible for converting an object to
JSON.

`JSONTransformer` takes a returned response object from a spark `Route` and
converts it to JSON.

Here's what registering a route which accepts and returns JSON looks like.

    service.post(
        "/validateMove",
        "application/json",
        new ValidateMoveRoute(gson),
        new JSONTransformer(gson));

`ValidateMoveRoute` implements `JSONRoute<Move>` and returns a response with a
`Message` object.  

    class ValidateMoveRoute extends JSONRoute<Move> {
      @Override
      Message handleJson(Request request, Move move, Response response) {
        // Do Stuff...
        return new Message("That move is OK!", Type.info);
      }
    }

Interactions around Sessions are abstracted away using the `SessionWrapper`
abstract class. This constrains what can be put into and taken out of sessions
introduces a seam which makes code using sessions easier to test. There are two
implementations of `SessionWrapper`: `RequestSessionWrapper` which stores its
state in a spark `Session` and `MockSessionWrapper` which stores its state in a
POJO. Routes which take advantage of a `SessionWrapper` overload the handle
method with one which accepts a `SessionWrapper`. The overloaded method is
called from the other method with a `SessionWrapper` created from a `Request`.
Here's an example:

    public class StartGameRoute implements TemplateViewRoute {

      public ModelAndView handle(SessionWrapper session, Response response)
          throws Exception {
            // Get Current Player
            Player currentPlayer = session.getPlayer();
            if (currentPlayer == null) {
              // Invalid Current Player, Send Them To Sign In Page
              response.redirect("/signin");
              return null;
            }

            // ...
          }

          @Override
          public ModelAndView handle(Request request, Response response) throws Exception {
            SessionWrapper session = SessionWrapper.from(request);

            return handle(session, response);
          }
        }
      }
    }

This route is tested by creating a `MockSessionWrapper` and testing the
`handle(SessionWrapper, Response)` method. The response and request objects are
mocked using the spring framework's mock `MockHttpServletResponse` and
`MockHttpServletRequest` objects.

    SessionWrapper session = new MockSessionWrapper(player1);

    StartGameRoute route = new StartGameRoute(/* ... */);
    MockHttpServletResponse response = new MockHttpServletResponse();

    ModelAndView modelAndView =
        route.handle(session, RequestResponseFactory.create(response));

Various classes handle displaying data about different parts of the application.
The responsibilities of each class are documented in the JavaDoc of each class.

#### Static models
![A UML Diagram of the View Tier of the Application](./ui-uml.png)

#### Dynamic models

![A Sequence Diagram of GetGameRoute](./game-route-sequence.jpg)

### Application Tier

The application tier holds dynamic state which lives through the lifetime of the
application.

It holds the `PlayerLobby` which is responsible for holding a list of signed in
players.

#### Static models
![PlayerLobby Diagram](./player-lobby-uml.png)

#### Dynamic models
![A Sequence Diagram of GetGameRoute](./game-route-sequence.jpg)

### Model Tier

The model tier holds containers for state an behavior for representing a game,
it's players and mutations which could be made to the game.

The `Board` class which is responsible for keeping track of each of the 64 tiles
on the game board, identifying whether a tile is white or black, mutating a
position on the board, accessing pieces on the board and creating a starting
board.

The `Player` class holds information about a player including their name, the
games they are participating in and the last validated moves they made pertaining
to the game.

The `Game` class ties `Player`s and a `Board` together. It handles holding the
information about which player its turn it currently is, whether the game is
over and the winner and looser.

There are a collection of classes in the `view` subpackage which help convert a
`Game` to something accepted by the view. It satisfies the contract for the
`game.ftl`.

There are a collection of class in the `rule` subpackage which contain logic for
checking whether a `Move` is valid or not. Classes complete a `Rule` abstract
class which takes in a Move, the color of the player making the move, and the
board the move is being made on. It returns the reason the move is invalid if
any. A `MoveValidator` checks whether a move is valid against a set of `Move`s.
This allows for switching rules at a granular level based on various factors.
These are leveraged to implement a different set of rules for jump moves.

#### Static models
![Some Rules](rule-uml.png)

![Game and Related Classes](game-uml.png)

#### Dynamic models
![A Sequence Diagram of GetGameRoute](./game-route-sequence.jpg)

[rules]: http://www.se.rit.edu/~swen-261/projects/WebCheckers/American%20Rules.html
